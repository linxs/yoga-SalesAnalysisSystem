package com.linxs.topcham.intefaces.controller.admin;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 
 * @author Linxs
 *
 */
@Controller
@RequestMapping(value = "/admin") 
public class AdminController {

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String admin(Model model) {
		
		return "admin/index";
	}
	
	@RequestMapping(value = "/sign-out", method = RequestMethod.GET)
	public String signOut(HttpServletRequest request) {
		request.getSession().invalidate();
		return "redirect:/";
	}
	
}
