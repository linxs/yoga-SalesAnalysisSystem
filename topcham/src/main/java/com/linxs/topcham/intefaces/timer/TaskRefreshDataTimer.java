package com.linxs.topcham.intefaces.timer;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.primitives.Ints;
import com.linxs.topcham.application.fnas.FansService;
import com.linxs.topcham.application.fnas.KdtapiService;
import com.linxs.topcham.application.org.AgencyOrgService;
import com.linxs.topcham.domain.fans.Fans;
import com.linxs.topcham.domain.kdtapi.KdtFans;
import com.linxs.topcham.domain.org.AgencyOrg;
import com.linxs.topcham.intefaces.utils.SpringUtil;

public class TaskRefreshDataTimer extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private Logger log = LoggerFactory.getLogger(TaskRefreshDataTimer.class);

	private FansService fansService;
	private AgencyOrgService orgService;
	private KdtapiService kdtapiService;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		Timer timer = new Timer();
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				try {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					Calendar ca = Calendar.getInstance();
					ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));
					String last = format.format(ca.getTime());

					Date today = new Date();
					if (new SimpleDateFormat("yyyy-MM-dd").format(today).equals(last)) {
						String currentHour = new SimpleDateFormat("HH").format(new Date());

						if (Ints.tryParse(currentHour) >= 23) {
							return;
						}

					}

					log.info("【平台粉丝数据自动同步任务启动】");

					fansService = (FansService) SpringUtil.getBean("fansServiceImpl");
					orgService = (AgencyOrgService) SpringUtil.getBean("agencyOrgServiceImpl");
					kdtapiService = (KdtapiService) SpringUtil.getBean("kdtApiServiceImpl");

//					kdtapiService.deleteApiFansData();
					List<Fans> fanss = kdtapiService.query();
					if(fanss.isEmpty()) {
						log.info("口袋通接口无粉丝数据返回，同步任务正常中止！");   
						return;
					}
					for (Fans fans : fanss) {
						
						if(kdtapiService.isExist(fans.getWeixinOpenId()))	{
							continue;
						}
						
						KdtFans kdtFans = new KdtFans();
						kdtFans.setAvatar(fans.getAvatar());
						kdtFans.setCity(fans.getCity());
						kdtFans.setFollowTime(fans.getFollowTime());
						kdtFans.setNick(fans.getNick());
						kdtFans.setProvince(fans.getProvince());
						kdtFans.setSex(fans.getSex());
						kdtFans.setUserId(fans.getUserId());
						kdtFans.setWeixinOpenId(fans.getWeixinOpenId());
						kdtFans.setTags(fans.getTags());
						try {
							kdtapiService.save(kdtFans);
							
						} catch (Exception e) {
							kdtFans.setNick(new String(kdtFans.getNick().getBytes(), "GBK"));
							kdtapiService.save(kdtFans);
						}
					}

//					fansService.removeAll();
					List<AgencyOrg> orgs = orgService.queryHaveTagsOrgs();
					for (AgencyOrg org : orgs) {
						List<KdtFans> fans = kdtapiService.queryByTags(org.getTags());
						fansService.relevanceFansToOrg(fans, org);
					}

					log.info("【平台粉丝数据自动同步成功！】");

				} catch (Exception e) {
					log.error("【平台粉丝数据自动同步异常】 " + e);
				}
			}

		}, 1000 * 60, (1000 * 60) * 30);

	}
}
