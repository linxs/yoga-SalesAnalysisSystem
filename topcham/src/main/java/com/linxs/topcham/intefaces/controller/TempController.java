package com.linxs.topcham.intefaces.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Properties;

import org.apache.http.HttpResponse;
import org.json.JSONObject;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.linxs.topcham.application.fnas.impl.KdtApiClient;

@Controller
public class TempController {

	@RequestMapping(value = "/queryweixintag", method = RequestMethod.GET)
	public String goquery() {
		return "temp";
	}
	
	@RequestMapping(value = "/queryweixintag", method = RequestMethod.POST) 
	public String temp(Model model, @RequestParam("id") String id) {
		String method = "kdt.users.weixin.follower.get";  //根据微信ID获取用户信息
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("fields", "user_id,weixin_openid,nick,avatar,follow_time,sex,province,city,tags");		
		params.put("user_id", id);
		KdtApiClient kdtApiClient;
		HttpResponse response;

		try {
			Properties props = new Properties();
			props = PropertiesLoaderUtils.loadAllProperties("kdtapp.properties");
			kdtApiClient = new KdtApiClient(props.getProperty("APP_ID"), props.getProperty("APP_SECRET"));

			response = kdtApiClient.get(method, params);
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = bufferedReader.readLine()) != null) {
				result.append(line);
			}

			JSONObject json = new JSONObject(result.toString());
			model.addAttribute("result", json.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "temp";
	}
	
}
