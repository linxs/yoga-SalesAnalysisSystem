package com.linxs.topcham.intefaces.controller.admin;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.common.primitives.Ints;
import com.kissme.core.orm.Page;
import com.linxs.topcham.application.fnas.KdtapiService;
import com.linxs.topcham.domain.fans.Order;

@Controller
@RequestMapping(value = "/admin") 
public class OrderController {
	
	@Autowired
	private KdtapiService apiService;

	@RequestMapping(value = "/order", method = RequestMethod.GET)
	public String list(Model model, HttpServletRequest request) {
		
		Page<Order> page = new Page<Order>();
		int pageNo = WebUtils.getCleanParam(request, "pageNo") == null ? 0 : Ints.tryParse(WebUtils.getCleanParam(request, "pageNo"));
		page = apiService.queryOrderPage(page.setPageNo(pageNo).setPageSize(10));
		model.addAttribute("page", page);
		
		
		return "/order/list";
	}
	
}
