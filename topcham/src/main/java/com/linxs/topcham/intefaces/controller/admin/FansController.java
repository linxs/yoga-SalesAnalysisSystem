package com.linxs.topcham.intefaces.controller.admin;

import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Maps;
import com.google.common.primitives.Ints;
import com.kissme.core.orm.Page;
import com.linxs.topcham.application.fnas.FansService;
import com.linxs.topcham.application.fnas.KdtapiService;
import com.linxs.topcham.application.org.AgencyOrgService;
import com.linxs.topcham.domain.fans.Fans;
import com.linxs.topcham.domain.kdtapi.KdtFans;
import com.linxs.topcham.domain.org.AgencyOrg;
import com.linxs.topcham.domain.user.User;

@Controller
@RequestMapping(value = "/admin")
public class FansController {

	@Autowired
	private FansService fansService;
	@Autowired
	private AgencyOrgService orgService;
	@Autowired
	private KdtapiService kdtapiService;

	@RequestMapping(value = "/fans", method = RequestMethod.GET)
	public String list(Model model, HttpServletRequest request) {
		try {

			Page<Fans> page = new Page<Fans>();
			int pageNo = WebUtils.getCleanParam(request, "pageNo") == null ? 0 : Ints.tryParse(WebUtils.getCleanParam(request, "pageNo"));

			String nick = WebUtils.getCleanParam(request, "nick") == null ? "" : WebUtils.getCleanParam(request, "nick");

			Map<String, Object> params = Maps.newHashMap();
			nick = URLDecoder.decode(nick, "UTF-8");
			params.put("nick", nick + "%");

			page.setParams(params);

			page = fansService.queryPage(page.setPageNo(pageNo).setPageSize(10));
			model.addAttribute("page", page);
			model.addAttribute("nick", nick);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/fans/list";
	}

	@RequestMapping(value = "/myfans", method = RequestMethod.GET)
	public String myFans(Model model, HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		Page<Fans> page = new Page<Fans>();
		int pageNo = WebUtils.getCleanParam(request, "pageNo") == null ? 0 : Ints.tryParse(WebUtils.getCleanParam(request, "pageNo"));
		page = fansService.queryMyFansPage(page.setPageNo(pageNo).setPageSize(10), user.getUsername());
		model.addAttribute("page", page);
		return "/fans/myFanslist";
	}

	@RequestMapping(value = "/addpersonmoney", method = RequestMethod.GET)
	public String addpersonmoney(Model model, HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		List<Fans> result = fansService.queryThisMonthNewAddFans(user.getUsername());
		model.addAttribute("result", result);

		Page<Fans> page = new Page<Fans>();
		int pageNo = WebUtils.getCleanParam(request, "pageNo") == null ? 0 : Ints.tryParse(WebUtils.getCleanParam(request, "pageNo"));
		page = fansService.queryMyThisMonthFansPage(page.setPageNo(pageNo).setPageSize(10), user.getUsername());
//		page = fansService.queryMyFansPage(page.setPageNo(pageNo).setPageSize(10), user.getUsername());
		model.addAttribute("page", page);

		return "/fans/addpersonmoney";
	}

	@RequestMapping(value = "/fans/refreshdata", method = RequestMethod.POST)
	@ResponseBody
	public String refreshData() {
		try {
//			fansService.removeAll();
			List<AgencyOrg> orgs = orgService.queryHaveTagsOrgs();
			for(AgencyOrg org : orgs) {
				List<KdtFans> fans = kdtapiService.queryByTags(org.getTags());
				fansService.relevanceFansToOrg(fans, org); 
			}
			return "success";
		} catch (Exception e) {
			return "fail";
		}
	}

	@RequestMapping(value = "/fans/refreshmydata", method = RequestMethod.POST)
	@ResponseBody
	public String refreshmydata() {
		try {
			fansService.removeAll();
			List<AgencyOrg> orgs = orgService.queryHaveTagsOrgs();
			for(AgencyOrg org : orgs) {
				List<KdtFans> fans = kdtapiService.queryByTags(org.getTags());
				fansService.relevanceFansToOrg(fans, org); 
			}
			return "success";
		} catch (Exception e) {
			return "fail";
		}
	}
	
	@RequestMapping(value = "/fans/activate", method = RequestMethod.POST) 
	@ResponseBody
	public String activate(@RequestParam("id") String id) {
		
		try {
			Fans fans = fansService.get(id);
			fansService.unactivate(fans);
			return "success";
		} catch (Exception e) {
			e.printStackTrace();
			return "fail";
		}
	}

	@RequestMapping(value = "/fans/unactivate", method = RequestMethod.POST) 
	@ResponseBody
	public String unactivate(@RequestParam("id") String id) {
		try {
			Fans fans = fansService.get(id);
			fansService.activate(fans);
			return "success";
		} catch (Exception e) { 
			e.printStackTrace();
			return "fail";
		}
	}

}
