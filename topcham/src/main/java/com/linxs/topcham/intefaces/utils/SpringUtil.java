package com.linxs.topcham.intefaces.utils;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringUtil {
	private static ApplicationContext applicationContext =  new ClassPathXmlApplicationContext("classpath:META-INF/spring/application-root.xml");

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public static void setApplicationContext(ApplicationContext applicationContext) {
		SpringUtil.applicationContext = applicationContext;
	}

	public static Object getBean(String name) {
		if (applicationContext == null) {
			return null;
		}
		return applicationContext.getBean(name);
	}

	public static Object getBean(ApplicationContext context, String name) {
		if (context == null) {
			return null;
		}
		return context.getBean(name);
	}
}
