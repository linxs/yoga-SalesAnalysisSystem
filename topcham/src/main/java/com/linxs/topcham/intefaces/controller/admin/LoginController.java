package com.linxs.topcham.intefaces.controller.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.linxs.topcham.application.org.AgencyOrgService;
import com.linxs.topcham.application.user.UserService;
import com.linxs.topcham.domain.org.AgencyOrg;
import com.linxs.topcham.domain.user.User;
import com.linxs.topcham.intefaces.utils.MD5Utils;

/**
 * 
 * @author Linxs
 * 
 */
@Controller
public class LoginController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private AgencyOrgService orgService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String login(Model model) {
		model.addAttribute("user", new User());
		return "/admin/login";
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	@ResponseBody
	public String login(Model model, User user, HttpServletRequest request, HttpServletResponse response) {

		User loginUser = login(user, request, response);
		if (loginUser != null) {
			request.getSession().setAttribute("user", loginUser);
			return "success";
		}

		return "fail";
	}

	private User login(User loginUser, HttpServletRequest request, HttpServletResponse response) {
		if ("admin".equals(loginUser.getUsername())) {

			loginUser.setPassword(new MD5Utils().generateMD5(loginUser.getPassword()));
			User user = userService.getByUsername(loginUser.getUsername());
			if (null != user) {
				if (!new MD5Utils().generateMD5(user.getPassword()).equals(new MD5Utils().generateMD5(loginUser.getPassword()))) {
					return null;
				}
				user.setRole("admin");
				return user;
			}
			
			
		} else {
			
			loginUser.setPassword(new MD5Utils().generateMD5(loginUser.getPassword()));
			AgencyOrg org = orgService.getByAccount(loginUser.getUsername());
			if (null != org) {
				if (!new MD5Utils().generateMD5(org.getPassword()).equals(new MD5Utils().generateMD5(loginUser.getPassword()))) {
					return null;
				}
				
				User user = new User();
				user.setId(org.getId()); 
				user.setUsername(org.getAccount()); 
				user.setRole("org");
				return user;
			}
			
		}
		return null;
	}

}
