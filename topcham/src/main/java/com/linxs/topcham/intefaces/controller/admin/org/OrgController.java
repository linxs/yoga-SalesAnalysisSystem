package com.linxs.topcham.intefaces.controller.admin.org;

import java.net.URLDecoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Maps;
import com.google.common.primitives.Ints;
import com.kissme.core.orm.Page;
import com.linxs.topcham.application.fnas.FansService;
import com.linxs.topcham.application.fnas.KdtapiService;
import com.linxs.topcham.application.org.AgencyOrgService;
import com.linxs.topcham.domain.org.AgencyOrg;
import com.linxs.topcham.intefaces.utils.MD5Utils;

@Controller
@RequestMapping(value = "/admin")
public class OrgController {

	@Autowired
	private AgencyOrgService agencyOrgService;
	@Autowired
	private KdtapiService apiService;
	@Autowired
	private FansService fansService;

	@RequestMapping(value = "/org", method = RequestMethod.GET)
	public String list(Model model, HttpServletRequest request) {
		try {
			Page<AgencyOrg> page = new Page<AgencyOrg>();
			int pageNo = WebUtils.getCleanParam(request, "pageNo") == null ? 0 : Ints.tryParse(WebUtils.getCleanParam(request, "pageNo"));

			String name = WebUtils.getCleanParam(request, "name") == null ? "" : WebUtils.getCleanParam(request, "name");
			String phone = WebUtils.getCleanParam(request, "phone") == null ? "" : WebUtils.getCleanParam(request, "phone");
			String areas = WebUtils.getCleanParam(request, "areas") == null ? "" : WebUtils.getCleanParam(request, "areas");

			Map<String, Object> params = Maps.newHashMap();
			name = URLDecoder.decode(name, "UTF-8");
			phone = URLDecoder.decode(phone, "UTF-8");
			areas = URLDecoder.decode(areas, "UTF-8");
			params.put("name", name + "%");
			params.put("phone", phone + "%");
			params.put("areas", areas + "%");

			page.setParams(params);
			page = agencyOrgService.queryPage(page.setPageNo(pageNo).setPageSize(10));
			model.addAttribute("page", page);
			model.addAttribute("name", name);
			model.addAttribute("phone", phone);
			model.addAttribute("areas", areas);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/org/list";
	}

	@RequestMapping(value = "/org/create", method = RequestMethod.GET)
	public String create(Model model) {
		model.addAttribute("org", new AgencyOrg());
		return "/org/form";
	}

	@RequestMapping(value = "/org/create", method = RequestMethod.POST)
	public String create(Model model, AgencyOrg org) {
		agencyOrgService.createAgencyOrg(org);
		return "redirect:/admin/org";
	}

	@RequestMapping(value = "/org/update/{id}", method = RequestMethod.GET)
	public String update(Model model, @PathVariable("id") String id) {
		model.addAttribute("org", agencyOrgService.get(id));
		return "/org/form";
	}

	@RequestMapping(value = "/org/update/{id}", method = RequestMethod.POST)
	public String update(Model model, AgencyOrg org) {
		agencyOrgService.updateAgencyOrg(org);
		return "redirect:/admin/org";
	}

	@RequestMapping(value = "/org/delete/{id}", method = RequestMethod.POST)
	@ResponseBody
	public String delete(@PathVariable("id") long id) {
		agencyOrgService.delete(id);
		return "success";
	}
	
	@RequestMapping(value = "/org/defaultpass", method = RequestMethod.POST) 
	@ResponseBody
	public String defaultPassword(@RequestParam("id") String id) {
		try {
			AgencyOrg org = agencyOrgService.get(id);
			org.setPassword(new MD5Utils().generateMD5("123456"));
			agencyOrgService.resetAgencyOrgPassword(org);
			return "success";
		} catch (Exception e) {
			return "fail";
		}
	}

	@RequestMapping(value = "/org/exists/{account}", method = RequestMethod.GET)
	@ResponseBody
	public String existsAgencyOrgAccount(@PathVariable("account") String account) {
		if (agencyOrgService.isExistsAccount(account)) {
			return "exists";
		}
		return "notExists";
	}
}
