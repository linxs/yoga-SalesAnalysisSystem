package com.linxs.topcham.intefaces.timer;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;
import com.linxs.topcham.application.fnas.KdtapiService;
import com.linxs.topcham.application.task.OrderService;
import com.linxs.topcham.domain.fans.Order;
import com.linxs.topcham.intefaces.utils.SpringUtil;

public class TaskRefreshOrderTimer extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private KdtapiService kdtapiService;
	private OrderService orderService;

	private Logger log = LoggerFactory.getLogger(TaskRefreshOrderTimer.class);

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		Timer timer = new Timer();
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				try {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					Calendar ca = Calendar.getInstance();
					ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));
					String last = format.format(ca.getTime());

					Date today = new Date();
					if (new SimpleDateFormat("yyyy-MM-dd").format(today).equals(last)) {
						String currentHour = new SimpleDateFormat("HH").format(new Date());

						if (Ints.tryParse(currentHour) >= 23) {
							return;
						}

					}

					log.info("【平台订单自动同步任务启动】");

					kdtapiService = (KdtapiService) SpringUtil.getBean("kdtApiServiceImpl");
					orderService = (OrderService) SpringUtil.getBean("oderServiceImpl");
					List<Order> kdtOrders = kdtapiService.queryThisMonthAllStatusOrders();
//					List<Order> orders = orderService.queryCurrentMonthAllOrder();
					List<String> orderIds = orderService.queryCurrentMonthAllOrderIds();
					List<Order> saveResult = Lists.newArrayList();
					
					for(Order item : kdtOrders) {
						if(orderIds.contains(item.getOrderId())) {
							//库中已有，更新订单状态
							Order order = orderService.getOrder(item.getOrderId());
//							if(!order.getStatus().equals(item.getStatus())) {
								orderService.updateOrderStatus(item);
//							}
						} else {
							//库中没有，增量添加
							saveResult.add(item);
						}
					}
					
					for (Order item : saveResult) {
						try {
							orderService.save(item);

						} catch (Exception e) {
							item.setFansNick(new String(item.getFansNick().getBytes(), "GBK"));
							try {
								orderService.save(item);
							} catch (Exception e2) {
								item.setReceiverName(new String(item.getReceiverName().getBytes(), "GBK"));
								orderService.save(item);
							}
						}
					}

					log.info("【平台订单自动同步成功！】");

				} catch (Exception e) {
					log.error("【平台订单自动同步异常】 " + e);
					e.printStackTrace();
				}
			}

		}, 1000 * 60, (1000 * 60) * 60);

	}

}
