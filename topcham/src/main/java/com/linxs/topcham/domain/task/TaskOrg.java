package com.linxs.topcham.domain.task;

import com.kissme.core.domain.AbstractDomain;
import com.linxs.topcham.domain.org.AgencyOrg;

public class TaskOrg extends AbstractDomain {

	private static final long serialVersionUID = 1L;

	private String orgId;
	private String addpersonMoney;
	private String saleCommission;
	private String month;
	private AgencyOrg org;

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getAddpersonMoney() {
		return addpersonMoney;
	}

	public void setAddpersonMoney(String addpersonMoney) {
		this.addpersonMoney = addpersonMoney;
	}

	public String getSaleCommission() {
		return saleCommission;
	}

	public void setSaleCommission(String saleCommission) {
		this.saleCommission = saleCommission;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public AgencyOrg getOrg() {
		return org;
	}

	public void setOrg(AgencyOrg org) {
		this.org = org;
	}

}
