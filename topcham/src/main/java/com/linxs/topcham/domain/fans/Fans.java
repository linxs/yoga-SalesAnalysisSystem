package com.linxs.topcham.domain.fans;

import java.util.Date;

import com.kissme.core.domain.AbstractDomain;
import com.linxs.topcham.domain.org.AgencyOrg;

public class Fans extends AbstractDomain {

	private static final long serialVersionUID = 1L;

	private String weixinOpenId;
	private String followTime;
	private String sex;
	private String nick;
	private String province;
	private String userId;
	private String avatar;
	private String city;
	private AgencyOrg org;
	private String orgId;
	private Date createTime;
	private String tags;
	private String status;

	public String getStatus() {
		return status;
	}

	public Fans setStatus(String status) {
		this.status = status;
		return this;
	}

	public String getTags() {
		return tags;
	}

	public Fans setTags(String tags) {
		this.tags = tags;
		return this;
	}

	public String getWeixinOpenId() {
		return weixinOpenId;
	}

	public Fans setWeixinOpenId(String weixinOpenId) {
		this.weixinOpenId = weixinOpenId;
		return this;
	}

	public String getFollowTime() {
		return followTime;
	}

	public Fans setFollowTime(String followTime) {
		this.followTime = followTime;
		return this;
	}

	public String getSex() {
		return sex;
	}

	public Fans setSex(String sex) {
		this.sex = sex;
		return this;
	}

	public String getNick() {
		return nick;
	}

	public Fans setNick(String nick) {
		this.nick = nick;
		return this;
	}

	public String getProvince() {
		return province;
	}

	public Fans setProvince(String province) {
		this.province = province;
		return this;
	}

	public String getUserId() {
		return userId;
	}

	public Fans setUserId(String userId) {
		this.userId = userId;
		return this;
	}

	public String getAvatar() {
		return avatar;
	}

	public Fans setAvatar(String avatar) {
		this.avatar = avatar;
		return this;
	}

	public String getCity() {
		return city;
	}

	public Fans setCity(String city) {
		this.city = city;
		return this;
	}

	public AgencyOrg getOrg() {
		return org;
	}

	public Fans setOrg(AgencyOrg org) {
		this.org = org;
		return this;
	}

	public String getOrgId() {
		return orgId;
	}

	public Fans setOrgId(String orgId) {
		this.orgId = orgId;
		return this;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public Fans setCreateTime(Date createTime) {
		this.createTime = createTime;
		return this;
	}

}
