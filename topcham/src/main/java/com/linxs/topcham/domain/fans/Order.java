package com.linxs.topcham.domain.fans;

import java.math.BigDecimal;

import com.kissme.core.domain.AbstractDomain;

public class Order extends AbstractDomain {

	private static final long serialVersionUID = 1L;
	private String orderId;
	private BigDecimal payment;
	private String fansNick;
	private BigDecimal commission;
	private String payTime;
	private String title;
	private String status;
	private String recordTime;
	private BigDecimal paymentCommission;
	private String weixinUserId;
	private String base; // 0.5 or 0.3
	private String orgId;
	private String address;
	private String receiverName;
	private String receiverMobile;

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getReceiverMobile() {
		return receiverMobile;
	}

	public void setReceiverMobile(String receiverMobile) {
		this.receiverMobile = receiverMobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getWeixinUserId() {
		return weixinUserId;
	}

	public void setWeixinUserId(String weixinUserId) {
		this.weixinUserId = weixinUserId;
	}

	public BigDecimal getPaymentCommission() {
		return paymentCommission;
	}

	public void setPaymentCommission(BigDecimal paymentCommission) {
		this.paymentCommission = paymentCommission;
	}

	public String getRecordTime() {
		return recordTime;
	}

	public void setRecordTime(String recordTime) {
		this.recordTime = recordTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public BigDecimal getPayment() {
		return payment;
	}

	public void setPayment(BigDecimal payment) {
		this.payment = payment;
	}

	public String getFansNick() {
		return fansNick;
	}

	public void setFansNick(String fansNick) {
		this.fansNick = fansNick;
	}

	public BigDecimal getCommission() {
		return commission;
	}

	public void setCommission(BigDecimal commission) {
		this.commission = commission;
	}

	public String getPayTime() {
		return payTime;
	}

	public void setPayTime(String payTime) {
		this.payTime = payTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
