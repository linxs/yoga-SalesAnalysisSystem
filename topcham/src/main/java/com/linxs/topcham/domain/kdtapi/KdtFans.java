package com.linxs.topcham.domain.kdtapi;

import com.kissme.core.domain.AbstractDomain;
import com.linxs.topcham.domain.org.AgencyOrg;

public class KdtFans extends AbstractDomain {

	private static final long serialVersionUID = 1L;

	private String weixinOpenId;
	private String followTime;
	private String sex;
	private String nick;
	private String province;
	private String userId;
	private String avatar;
	private String city;
	private AgencyOrg org;
	private String tags;

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getWeixinOpenId() {
		return weixinOpenId;
	}

	public void setWeixinOpenId(String weixinOpenId) {
		this.weixinOpenId = weixinOpenId;
	}

	public String getFollowTime() {
		return followTime;
	}

	public void setFollowTime(String followTime) {
		this.followTime = followTime;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public AgencyOrg getOrg() {
		return org;
	}

	public void setOrg(AgencyOrg org) {
		this.org = org;
	}

}
