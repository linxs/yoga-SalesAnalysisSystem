package com.linxs.topcham.domain.org;

import java.math.BigDecimal;

import com.kissme.core.domain.AbstractDomain;

public class AgencyOrg extends AbstractDomain {

	private static final long serialVersionUID = 1L;

	private String orgName;
	private String account;
	private String password;
	private String contactMan;
	private String phone;
	private String email;
	private String address;
	private double addPersonMoney;
	private BigDecimal salesCommissions;
	private String status;
	private String areas;
	private String tags;

	public String getTags() {
		return tags;
	}

	public AgencyOrg setTags(String tags) {
		this.tags = tags;
		return this;
	}

	public String getAreas() {
		return areas;
	}

	public AgencyOrg setAreas(String areas) {
		this.areas = areas;
		return this;
	}

	public String getOrgName() {
		return orgName;
	}

	public AgencyOrg setOrgName(String orgName) {
		this.orgName = orgName;
		return this;
	}

	public String getAccount() {
		return account;
	}

	public AgencyOrg setAccount(String account) {
		this.account = account;
		return this;
	}

	public String getPassword() {
		return password;
	}

	public AgencyOrg setPassword(String password) {
		this.password = password;
		return this;
	}

	public String getContactMan() {
		return contactMan;
	}

	public AgencyOrg setContactMan(String contactMan) {
		this.contactMan = contactMan;
		return this;
	}

	public String getPhone() {
		return phone;
	}

	public AgencyOrg setPhone(String phone) {
		this.phone = phone;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public AgencyOrg setEmail(String email) {
		this.email = email;
		return this;
	}

	public String getAddress() {
		return address;
	}

	public AgencyOrg setAddress(String address) {
		this.address = address;
		return this;
	}

	public double getAddPersonMoney() {
		return addPersonMoney;
	}

	public void setAddPersonMoney(double addPersonMoney) {
		this.addPersonMoney = addPersonMoney;
	}

	public BigDecimal getSalesCommissions() {
		return salesCommissions;
	}

	public void setSalesCommissions(BigDecimal salesCommissions) {
		this.salesCommissions = salesCommissions;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
