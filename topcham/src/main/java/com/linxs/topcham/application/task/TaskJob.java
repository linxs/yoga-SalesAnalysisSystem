package com.linxs.topcham.application.task;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class TaskJob extends QuartzJobBean {

	private TaskService taskService;
	private Logger log = LoggerFactory.getLogger(TaskJob.class);
 
	public void execute() {
		log.info("[定时任务启动]");
		taskService.countOrgCurrentMonthMoney();
//		taskService.recordCurrentMonthOrders(); 
		log.info("[定时任务执行完成]");
	}

	@Override
	protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

}
