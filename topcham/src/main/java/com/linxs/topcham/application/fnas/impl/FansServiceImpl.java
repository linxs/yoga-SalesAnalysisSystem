package com.linxs.topcham.application.fnas.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.kissme.core.orm.Page;
import com.linxs.topcham.application.fnas.FansService;
import com.linxs.topcham.domain.fans.Fans;
import com.linxs.topcham.domain.kdtapi.KdtFans;
import com.linxs.topcham.domain.org.AgencyOrg;
import com.linxs.topcham.infrastruture.persist.fans.FansRepository;
import com.linxs.topcham.infrastruture.persist.org.AgencyOrgRepository;

@Service
public class FansServiceImpl implements FansService {

	@Autowired
	private AgencyOrgRepository orgRepository;
	@Autowired
	private FansRepository fansRepository; 
	
	@Override
	public void relevanceFansToOrg(Fans fans) {
		fans.setCreateTime(new Date()); 
		fansRepository.saveFans(fans); 
	}

	@Override
	public Page<Fans> queryPage(Page<Fans> page) {
		page = fansRepository.queryPage(page);
		List<Fans> result = Lists.newArrayList();
		for(Fans fans : page.getResult()) {
			AgencyOrg org = orgRepository.get(Long.valueOf(fans.getOrgId()));
			fans.setOrg(org);
			boolean status = fansRepository.existActivate(fans.getUserId());
			fans.setStatus(status ? "0" : "1");
			
			result.add(fans);
		}
		page.setResult(result);
		return page;
	}

	@Override
	public Page<Fans> queryMyFansPage(Page<Fans> page, String username) {
		return fansRepository.queryMyFansPage(page, username);
	}

	@Override
	public List<Fans> queryMyFans(String username) {
		return fansRepository.queryMyFans(username); 
	}

	@Override
	public List<Fans> queryThisMonthNewAddFans(String username) {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
        Calendar c = Calendar.getInstance();    
        c.add(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH,1);
        String first = format.format(c.getTime());
        
        Calendar ca = Calendar.getInstance();    
        ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));  
        String last = format.format(ca.getTime());
         
		return fansRepository.queryMyFans(username, first, last);
	}

	@Override
	public Fans getFansByUserid(String userid) {
		return fansRepository.getFansByUserid(userid);
	}

	@Override
	public void relieve(String id) {
		fansRepository.delete(id);
	}

	@Override
	public void relevanceFansToOrg(List<KdtFans> result, AgencyOrg org) {
		for(KdtFans kdtFans : result) {
			
			Fans oldFans = fansRepository.getFansByOrgAndWeixinOpenId(kdtFans.getWeixinOpenId(), org.getId());
			if(null != oldFans) {
				continue;
			}
			
			Fans fans = new Fans();
			fans.setAvatar(kdtFans.getAvatar())
				.setCity(kdtFans.getCity())
				.setFollowTime(kdtFans.getFollowTime())
				.setNick(kdtFans.getNick())
				.setOrg(org)
				.setOrgId(org.getId())
				.setProvince(kdtFans.getProvince())
				.setSex(kdtFans.getSex())
				.setTags(kdtFans.getTags())
				.setUserId(kdtFans.getUserId())
				.setWeixinOpenId(kdtFans.getWeixinOpenId()) 
				.setStatus("1") 
				.setCreateTime(new Date());
				
			fansRepository.saveFans(fans);
		}
	}

	@Override
	public void removeAll() {
		fansRepository.removeAll();
	}

	@Override
	public Fans get(String id) {
		return fansRepository.get(Long.valueOf(id)); 
	}

	@Override
	public void activate(Fans fans) {
		fansRepository.activate(fans);
	}

	@Override
	public void unactivate(Fans fans) {
		fansRepository.unactivate(fans);
	}

	@Override
	public Page<Fans> queryMyThisMonthFansPage(Page<Fans> page, String username) {
		return fansRepository.queryMyThisMonthFansPage(page, username);
	}

}
