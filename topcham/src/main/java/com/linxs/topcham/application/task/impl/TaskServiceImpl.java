package com.linxs.topcham.application.task.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.linxs.topcham.application.fnas.KdtapiService;
import com.linxs.topcham.application.org.AgencyOrgService;
import com.linxs.topcham.application.task.TaskService;
import com.linxs.topcham.domain.fans.Order;
import com.linxs.topcham.domain.org.AgencyOrg;
import com.linxs.topcham.domain.task.TaskOrg;
import com.linxs.topcham.infrastruture.persist.task.OrderRepository;
import com.linxs.topcham.infrastruture.persist.task.TaskRepository;

@Service
public class TaskServiceImpl implements TaskService {

	@Autowired
	private AgencyOrgService orgService;
	@Autowired
	private TaskRepository taskRepository;
	@Autowired
	private KdtapiService apiService;
	@Autowired
	private OrderRepository orderRepository;

	private Logger log = LoggerFactory.getLogger(TaskServiceImpl.class);

	@Override
	public void countOrgCurrentMonthMoney() {
		try {

			log.info("【开始计算本月推广机构增员费用及销售提成】");
			List<AgencyOrg> result = orgService.queryCurrentMonth();
//			List<AgencyOrg> result = orgService.queryCurrentMonthFormPersiterData();
			for (AgencyOrg org : result) {
				
				BigDecimal commissions = countSaleCommission(org);
				
				org.setSalesCommissions(commissions); 
				
				TaskOrg taskOrg = new TaskOrg();
				taskOrg.setAddpersonMoney(String.valueOf(org.getAddPersonMoney()));
				taskOrg.setOrgId(org.getId());
				taskOrg.setMonth(new SimpleDateFormat("yyyy-MM").format(new Date()));
				taskOrg.setSaleCommission(String.valueOf(org.getSalesCommissions()));
				try {
					taskRepository.save(taskOrg);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			log.info("【本月推广机构增员费用及销售提成计算完成】");
		} catch (Exception e) {
			log.error("[推广机构增员费用及销售提成计算异常！] ", e.getMessage());
			e.printStackTrace();
		}
	}

	private BigDecimal countSaleCommission(AgencyOrg org) {
		List<Order> orders = orderRepository.queryFinishOrderOfOrg(org.getId());
		
		for(Order order : orders) {
			if (null != org) {
				if (order.getAddress().indexOf(org.getAreas()) != -1 || "全国地区".equals(org.getAreas())) {
					order.setBase("0.05");
					order.setCommission(new BigDecimal("0.05").setScale(2, BigDecimal.ROUND_DOWN).multiply(order.getPayment())
							.setScale(2, BigDecimal.ROUND_DOWN));
				} else {
					order.setBase("0.03");
					order.setCommission(new BigDecimal("0.03").setScale(2, BigDecimal.ROUND_DOWN).multiply(order.getPayment())
							.setScale(2, BigDecimal.ROUND_DOWN));
				}
				order.setOrgId(org.getId());
			} else {
				order.setBase("0.03");
				order.setCommission(new BigDecimal("0.03").setScale(2, BigDecimal.ROUND_DOWN).multiply(order.getPayment())
						.setScale(2, BigDecimal.ROUND_DOWN));

			}
		}
		
		
		BigDecimal commissions = new BigDecimal("0.0").setScale(2, BigDecimal.ROUND_DOWN);
		for(Order o : orders) {
			commissions = commissions.add(o.getCommission());
		}
		return commissions;
	}

	@Override
	public void recordCurrentMonthOrders() {
		log.info("[开始记录本月所有状态的订单]");
		List<Order> orders = apiService.queryThisMonthAllStatusOrders();
		String recordTime = new SimpleDateFormat("yyyy-MM").format(new Date());
		for(Order order : orders) {
			order.setRecordTime(recordTime); 
			orderRepository.save(order);
		}
		log.info("[本月所有状态订单记录完成]");
	}

}
