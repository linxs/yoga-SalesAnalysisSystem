package com.linxs.topcham.application.fnas;

import java.util.List;

import com.kissme.core.orm.Page;
import com.linxs.topcham.domain.fans.Fans;
import com.linxs.topcham.domain.fans.Order;
import com.linxs.topcham.domain.kdtapi.KdtFans;

public interface KdtapiService {
	
	Page<KdtFans> query(Page<KdtFans> page);
 
	List<Order> queryThisMonthOrders(String areas, String orderStatus);  

	void deleteApiFansData(); 

	List<Fans> query();

	void save(KdtFans kdtFans); 

	Page<Order> queryOrderPage(Page<Order> page);

	List<Order> queryThisMonthAllStatusOrders();

	List<KdtFans> queryByTags(String tags); 

	List<Order> queryThisMonthOrders(String orderStatus);

	boolean isExist(String weixinOpenId);   

}
