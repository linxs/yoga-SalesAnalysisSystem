package com.linxs.topcham.application.task.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.linxs.topcham.application.task.OrderService;
import com.linxs.topcham.domain.fans.Order;
import com.linxs.topcham.infrastruture.persist.task.OrderRepository;

@Service
public class OderServiceImpl implements OrderService {

	@Autowired
	private OrderRepository orderRepository;

	@Override
	public Order get(String id) {
		return orderRepository.get(id);
	}

	@Override
	public void update(Order order) {
		orderRepository.update(order);
	}

	@Override
	public void save(Order item) {
		orderRepository.save(item);
	}

	@Override
	public List<Order> queryCurrentMonthAllOrder() {
		return orderRepository.queryCurrentMonthAllOrder();
	}

	@Override
	public List<String> queryCurrentMonthAllOrderIds() {
		return orderRepository.queryCurrentMonthAllOrderIds();
	}

	@Override
	public Order getOrder(String orderId) {
		return orderRepository.getOrder(orderId);
	}

	@Override
	public void updateOrderStatus(Order order) {
		orderRepository.updateOrderStatus(order);
	}

	@Override
	public List<Order> queryFinishOrderOfOrg(String id) {
		return orderRepository.queryFinishOrderOfOrg(id);
	}

	@Override
	public void updateAddress(Order tid) {
		orderRepository.updateAddress(tid);
	}

	@Override
	public List<Order> queryAllOrder() {
		return orderRepository.queryAllOrder();
	}
	
}
