package com.linxs.topcham.application.fnas;

import java.util.List;

import com.kissme.core.orm.Page;
import com.linxs.topcham.domain.fans.Fans;
import com.linxs.topcham.domain.kdtapi.KdtFans;
import com.linxs.topcham.domain.org.AgencyOrg;

public interface FansService {

	void relevanceFansToOrg(Fans fans);

	Page<Fans> queryPage(Page<Fans> page);

	Page<Fans> queryMyFansPage(Page<Fans> setPageSize, String username);

	List<Fans> queryMyFans(String id);

	List<Fans> queryThisMonthNewAddFans(String username);

	Fans getFansByUserid(String userid);

	void relieve(String id);
 
	void relevanceFansToOrg(List<KdtFans> fans, AgencyOrg org);

	void removeAll(); 

	Fans get(String id);

	void activate(Fans fans);

	void unactivate(Fans fans); 

	Page<Fans> queryMyThisMonthFansPage(Page<Fans> page, String username);    

}
