package com.linxs.topcham.application.task;

import java.util.List;

import com.linxs.topcham.domain.fans.Order;

public interface OrderService {

	Order get(String id);
 
	void update(Order order);

	void save(Order item);

	List<Order> queryCurrentMonthAllOrder();

	List<String> queryCurrentMonthAllOrderIds();

	Order getOrder(String orderId);

	void updateOrderStatus(Order order);

	List<Order> queryFinishOrderOfOrg(String id);
 
	void updateAddress(Order tid);

	List<Order> queryAllOrder();      

}
