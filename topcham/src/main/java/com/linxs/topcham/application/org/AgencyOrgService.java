package com.linxs.topcham.application.org;

import java.io.UnsupportedEncodingException;
import java.util.List;

import com.kissme.core.orm.Page;
import com.linxs.topcham.domain.org.AgencyOrg;

public interface AgencyOrgService {

	AgencyOrg createAgencyOrg(AgencyOrg org);
	
	AgencyOrg updateAgencyOrg(AgencyOrg org);
	
	AgencyOrg resetAgencyOrgPassword(AgencyOrg org);
	
	Page<AgencyOrg> queryPage(Page<AgencyOrg> page);
	 
	void relevanceFans(List<String> ids, long id);
	
	AgencyOrg loadAgencyOrgInfo(long id);

	boolean isExistsAccount(String account);

	List<AgencyOrg> query();
 
	AgencyOrg getByAccount(String username);

	AgencyOrg get(String id); 
 
	void delete(long id);
 
	List<AgencyOrg> queryCurrentMonth();

	List<AgencyOrg> queryHaveTagsOrgs();

	AgencyOrg queryOrgByFansUserId(String weixinUserId); 

	List<AgencyOrg> queryCurrentMonthFormPersiterData();

	
}
