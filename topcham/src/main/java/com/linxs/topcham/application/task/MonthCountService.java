package com.linxs.topcham.application.task;

import java.util.List;

import com.kissme.core.orm.Page;
import com.linxs.topcham.domain.fans.Order;
import com.linxs.topcham.domain.task.TaskOrg;

public interface MonthCountService {

	Page<TaskOrg> queryOrgMonthRecord(Page<TaskOrg> page);

	Page<Order> queryOrderMonthRecord(Page<Order> setPageSize);

	TaskOrg queryByOrgAndMonth(String id, String recordTime);

	void update(TaskOrg task);

	List<Order> query();
 
	List<Order> queryByMonth(String month);  
 
} 
 