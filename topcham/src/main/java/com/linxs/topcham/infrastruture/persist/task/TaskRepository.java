package com.linxs.topcham.infrastruture.persist.task;

import com.kissme.core.orm.Page;
import com.linxs.topcham.domain.task.TaskOrg;

public interface TaskRepository {

	void save(TaskOrg taskOrg);

	Page<TaskOrg> queryOrgMonthRecord(Page<TaskOrg> page);

	TaskOrg queryByOrgAndMonth(String id, String recordTime);

	void update(TaskOrg task);  
 
}
 