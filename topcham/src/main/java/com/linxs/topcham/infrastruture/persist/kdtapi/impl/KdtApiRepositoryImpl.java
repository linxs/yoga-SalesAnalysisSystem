package com.linxs.topcham.infrastruture.persist.kdtapi.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.google.common.base.Strings;
import com.kissme.core.orm.Page;
import com.kissme.core.orm.mybatis.MybatisRepositorySupport;
import com.linxs.topcham.domain.kdtapi.KdtFans;
import com.linxs.topcham.infrastruture.persist.kdtapi.KdtApiRepository;

@Repository
public class KdtApiRepositoryImpl extends MybatisRepositorySupport<Long, KdtFans> implements KdtApiRepository {

	@Override
	protected String getNamespace() {
		return "com.me.kdtapi";
	}
	
	@Override
	public void delete() {
		getSqlSession().delete(getNamespace() + ".delete");
	}

	@Override
	public void save(KdtFans fans) {
		getSqlSession().insert(getNamespace() + ".save", fans);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<KdtFans> query(Page<KdtFans> page) {
		if(page.getParams().containsKey("nick")) {
			if(!Strings.isNullOrEmpty(String.valueOf(page.getParams().get("nick")))) {
				List<KdtFans> Commoditys = getSqlSession().selectList(getNamespace() + ".search", page);
				page.setResult(Commoditys);
				page.setTotalCount(getQueryCount(".search"));
				return page;
			}
		}
		
		List<KdtFans> Commoditys = getSqlSession().selectList(getNamespace() + ".query", page);
		page.setResult(Commoditys);
		page.setTotalCount(getQueryCount(".query"));
		return page;
	} 
	
	private int getQueryCount(String method) {
		return getSqlSession().selectList(getNamespace() + method).size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<KdtFans> queryByTags(String tags) {
		return getSqlSession().selectList(getNamespace() + ".queryByTags", tags); 
	}

	@Override
	public KdtFans get(String weixinOpenId) {
		KdtFans fans = (KdtFans) getSqlSession().selectOne(getNamespace() + ".get", weixinOpenId);
		return fans;
	}
 
	
	
}
