package com.linxs.topcham.infrastruture.persist.fans.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.kissme.core.orm.Page;
import com.kissme.core.orm.mybatis.MybatisRepositorySupport;
import com.linxs.topcham.domain.fans.Fans;
import com.linxs.topcham.domain.fans.FansQueryVo;
import com.linxs.topcham.infrastruture.persist.fans.FansRepository;

@Repository
public class FansRepositoryImpl extends MybatisRepositorySupport<Integer, Fans> implements FansRepository {

	@Override
	protected String getNamespace() {
		return "com.me.fans";
	}

	@Override
	public Fans saveFans(Fans org) {
		getSqlSession().insert(getNamespace() + ".save", org);
		return org;
	}

	@Override
	public Fans get(long id) {
		return (Fans) getSqlSession().selectOne(getNamespace() + ".get", id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<Fans> queryPage(Page<Fans> page) {

		if (!Strings.isNullOrEmpty(page.getParams().get("nick").toString().replace("%", ""))) {
			List<Fans> Commoditys = getSqlSession().selectList(getNamespace() + ".searchNick", page);
			page.setResult(Commoditys);
			page.setTotalCount(getQueryCount(".searchNick"));

		} else {

			List<Fans> Commoditys = getSqlSession().selectList(getNamespace() + ".query", page);
			page.setResult(Commoditys);
			page.setTotalCount(getQueryCount(".query"));
		}
		return page;
	}

	private int getQueryCount(String method) {
		return getSqlSession().selectList(getNamespace() + method).size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Fans> query() {
		return getSqlSession().selectList(getNamespace() + ".query");
	}

	@Override
	public int countFansWithOrg(@Param("id") String id) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, 0);
		c.set(Calendar.DAY_OF_MONTH, 1);
		String first = format.format(c.getTime());

		Calendar ca = Calendar.getInstance();
		ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));
		String last = format.format(ca.getTime());

		Map<String, String> params = Maps.newHashMap();
		params.put("id", id);
		params.put("first", first);
		params.put("last", last);

		return getSqlSession().selectList(getNamespace() + ".queryByOrgId", params).size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<Fans> queryMyFansPage(Page<Fans> page, String username) {

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("username", username);

		page.setParams(params);
		List<Fans> Commoditys = getSqlSession().selectList(getNamespace() + ".queryMyFansPage", page);
		page.setResult(Commoditys);

		page.setTotalCount(getSqlSession().selectList(getNamespace() + ".queryMyFansByUsername", username).size());
		return page;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Fans> queryMyFans(String username) {
		return getSqlSession().selectList(getNamespace() + ".queryMyFansPage", username);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Fans> queryMyFans(String username, String first, String last) {
		FansQueryVo vo = new FansQueryVo();
		vo.setFirst(first);
		vo.setLast(last);
		vo.setUsername(username);

		return getSqlSession().selectList(getNamespace() + ".queryThisMonthMyFans", vo);
	}

	@Override
	public Fans getFansByUserid(String userid) {
		return (Fans) getSqlSession().selectOne(getNamespace() + ".getFansByUserid", userid);
	}

	@Override
	public void delete(String id) {
		getSqlSession().delete(getNamespace() + ".delete", id);
	}

	@Override
	public void removeAll() {
		getSqlSession().delete(getNamespace() + ".removeAll");
	}

	@Override
	public void activate(Fans fans) {
		getSqlSession().insert(getNamespace() + ".activate", fans);
	}

	@Override
	public boolean existActivate(String userId) {
		int count = (Integer) getSqlSession().selectOne(getNamespace() + ".existActivate", userId);
		return count > 0 ? true : false;
	}

	@Override
	public void unactivate(Fans fans) {
		getSqlSession().delete(getNamespace() + ".unactivate", fans);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<Fans> queryMyThisMonthFansPage(Page<Fans> page, String username) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("username", username);

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, 0);
		c.set(Calendar.DAY_OF_MONTH, 1);
		String first = format.format(c.getTime());

		Calendar ca = Calendar.getInstance();
		ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));
		String last = format.format(ca.getTime());

		params.put("first", first);
		params.put("last", last);

		page.setParams(params);
		List<Fans> Commoditys = getSqlSession().selectList(getNamespace() + ".queryMyThisMonthFansPage", page);
		page.setResult(Commoditys);

		page.setTotalCount(getSqlSession().selectList(getNamespace() + ".queryMyFansByUsername", username).size());
		return page;
	}

	@Override
	public Fans getFansByOrgAndWeixinOpenId(String weixinOpenId, String orgId) {
		Fans fans = new Fans();
		fans.setOrgId(orgId);
		fans.setWeixinOpenId(weixinOpenId);

		return (Fans) getSqlSession().selectOne(getNamespace() + ".getFansByOrgAndWeixinOpenId", fans);
	}

}
