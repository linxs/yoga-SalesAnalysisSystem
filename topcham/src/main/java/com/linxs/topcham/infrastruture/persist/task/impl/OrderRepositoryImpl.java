package com.linxs.topcham.infrastruture.persist.task.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.google.common.base.Strings;
import com.kissme.core.orm.Page;
import com.kissme.core.orm.mybatis.MybatisRepositorySupport;
import com.linxs.topcham.domain.fans.Order;
import com.linxs.topcham.infrastruture.persist.task.OrderRepository;

@Repository
public class OrderRepositoryImpl extends MybatisRepositorySupport<Integer, Order> implements OrderRepository {

	@Override
	protected String getNamespace() {
		return "com.me.order";
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<Order> queryPage(Page<Order> page) {
		if (!Strings.isNullOrEmpty(page.getParams().get("orderid").toString()) && !Strings.isNullOrEmpty(page.getParams().get("month").toString())) {
			page.setResult(getSqlSession().selectList(getNamespace() + ".searchFull", page));
			page.setTotalCount(getSqlSession().selectList(getNamespace() + ".searchFull").size());
			
		} else if (!Strings.isNullOrEmpty(page.getParams().get("orderid").toString())) {
			page.setResult(getSqlSession().selectList(getNamespace() + ".searchOrder", page));
			page.setTotalCount(getSqlSession().selectList(getNamespace() + ".searchOrder").size());
			
		} else if (!Strings.isNullOrEmpty(page.getParams().get("month").toString())) {
			page.setResult(getSqlSession().selectList(getNamespace() + ".searchMonth", page));
			page.setTotalCount(getSqlSession().selectList(getNamespace() + ".searchMonth", page.getParams().get("month")).size());

		} else {
			page.setResult(getSqlSession().selectList(getNamespace() + ".query", page));
			page.setTotalCount(getSqlSession().selectList(getNamespace() + ".query").size());

		}
		return page;
	}

	@Override
	public void save(Order entity) {
		getSqlSession().insert(getNamespace() + ".save", entity);
	}

	@Override
	public void saveOrderMonthRecord(Order order) {
		getSqlSession().insert(getNamespace() + ".saveOrderMonthRecord", order);
	}

	@Override
	public Order get(String id) {
		return (Order) getSqlSession().selectOne(getNamespace() + ".get", id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Order> queryThisMonthOrders(String id, String recordTime) {
		Order order = new Order();
		order.setOrgId(id);
		order.setRecordTime(recordTime);
		return getSqlSession().selectList(getNamespace() + ".queryThisMonth", order);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Order> query() {
		return getSqlSession().selectList(getNamespace() + ".exporlQuery");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Order> queryCurrentMonthAllOrder() {
		return getSqlSession().selectList(getNamespace() + ".queryCurrentMonthAllOrder");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> queryCurrentMonthAllOrderIds() {
		return getSqlSession().selectList(getNamespace() + ".queryCurrentMonthAllOrderIds");
	}

	@Override
	public Order getOrder(String orderId) {
		Order order = new Order();
		order.setOrderId(orderId); 
		return (Order) getSqlSession().selectOne(getNamespace() + ".getOrder", order);
	}

	@Override
	public void updateOrderStatus(Order order) {
		getSqlSession().update(getNamespace() + ".updateOrderStatus", order);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Order> queryFinishOrderOfOrg(String id) {
		return getSqlSession().selectList(getNamespace() + ".queryFinishOrderOfOrg", id); 
	}

	@Override
	public void updateAddress(Order tid) {
		getSqlSession().update(getNamespace() + ".updateAddress", tid);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Order> queryAllOrder() {
		return getSqlSession().selectList(getNamespace() + ".queryAllOrder");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Order> queryCurrentMonthPersiterData() {
		return getSqlSession().selectList(getNamespace() + ".queryCurrentMonthPersiterData");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Order> queryByMonth(String month) { 
		return getSqlSession().selectList(getNamespace() + ".exporlQueryByMonth", month); 
	}
}
