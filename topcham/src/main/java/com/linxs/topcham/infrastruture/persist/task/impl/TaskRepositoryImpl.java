package com.linxs.topcham.infrastruture.persist.task.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.google.common.base.Strings;
import com.kissme.core.orm.Page;
import com.kissme.core.orm.mybatis.MybatisRepositorySupport;
import com.linxs.topcham.domain.task.TaskOrg;
import com.linxs.topcham.infrastruture.persist.task.TaskRepository;

@Repository
public class TaskRepositoryImpl extends MybatisRepositorySupport<Integer, TaskOrg> implements TaskRepository {

	@Override
	protected String getNamespace() {
		return "com.me.taskOrg";
	}
	
	@Override
	public void save(TaskOrg entity) {
		getSqlSession().insert(getNamespace() + ".save", entity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<TaskOrg> queryOrgMonthRecord(Page<TaskOrg> page) {
		
		if(page.getParams().containsKey("month") && page.getParams().containsKey("orgName")) {
			if(!Strings.isNullOrEmpty(String.valueOf(page.getParams().get("month"))) && !Strings.isNullOrEmpty(String.valueOf(page.getParams().get("orgName")))) {
				List<TaskOrg> Commoditys = getSqlSession().selectList(getNamespace() + ".searchByAll", page);
				page.setResult(Commoditys);
				page.setTotalCount(getSqlSession().selectList(getNamespace() + ".searchByAll").size());
				return page;
			}
		}
		
		if(page.getParams().containsKey("month")) {
			if(!Strings.isNullOrEmpty(String.valueOf(page.getParams().get("month")))) {
				List<TaskOrg> Commoditys = getSqlSession().selectList(getNamespace() + ".searchByMonth", page);
				page.setResult(Commoditys);   
				page.setTotalCount(getSqlSession().selectList(getNamespace() + ".searchByMonth", page.getParams().get("month")).size());
				return page;
			} 
		}
		
		if(page.getParams().containsKey("orgName")) {
			if(!Strings.isNullOrEmpty(String.valueOf(page.getParams().get("orgName")))) {
				List<TaskOrg> Commoditys = getSqlSession().selectList(getNamespace() + ".search", page);
				page.setResult(Commoditys);
				page.setTotalCount(getSqlSession().selectList(getNamespace() + ".search").size());
				return page;
			}
		}
		
		page.setResult(getSqlSession().selectList(getNamespace() + ".query", page));
		page.setTotalCount(getSqlSession().selectList(getNamespace() + ".query").size());
		return page;
	}

	@Override
	public TaskOrg queryByOrgAndMonth(String id, String recordTime) {
		TaskOrg org = new TaskOrg();
		org.setId(id); 
		org.setMonth(recordTime); 
		return (TaskOrg) getSqlSession().selectOne(getNamespace() + ".queryByOrgAndMonth", org); 
	} 
	
}
