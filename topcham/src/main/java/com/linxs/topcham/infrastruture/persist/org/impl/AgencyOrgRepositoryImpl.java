package com.linxs.topcham.infrastruture.persist.org.impl;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.google.common.base.Strings;
import com.kissme.core.orm.Page;
import com.kissme.core.orm.mybatis.MybatisRepositorySupport;
import com.linxs.topcham.domain.org.AgencyOrg;
import com.linxs.topcham.infrastruture.persist.org.AgencyOrgRepository;

@Repository
public class AgencyOrgRepositoryImpl extends MybatisRepositorySupport<Integer, AgencyOrg> implements AgencyOrgRepository {

	@Override
	protected String getNamespace() {
		return "com.me.org";
	}
	
	@Override
	public void save(AgencyOrg org) {
		getSqlSession().insert(getNamespace() + ".save", org);
	}

	@Override
	public void update(AgencyOrg org) {
		getSqlSession().update(getNamespace() + ".update", org);
	}

	@Override
	public AgencyOrg get(long id) { 
		return (AgencyOrg) getSqlSession().selectOne(getNamespace() + ".get", id);
	}

	@Override
	public AgencyOrg findAgencyOrgByAccount(@Param("account") String account) {
		
		return (AgencyOrg) getSqlSession().selectOne(getNamespace() + ".findAgencyOrgByAccount", account);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Page<AgencyOrg> queryPage(Page<AgencyOrg> page) {
		if(!Strings.isNullOrEmpty(page.getParams().get("name").toString().replace("%", "")) && !Strings.isNullOrEmpty(page.getParams().get("phone").toString().replace("%", "")) && !Strings.isNullOrEmpty(page.getParams().get("areas").toString().replace("%", ""))) {
			
			List<AgencyOrg> Commoditys = getSqlSession().selectList(getNamespace() + ".searchFull", page);
			page.setResult(Commoditys);
			page.setTotalCount(getQueryCount(".searchFull"));
		} else if (!Strings.isNullOrEmpty(page.getParams().get("name").toString().replace("%", ""))) {
			List<AgencyOrg> Commoditys = getSqlSession().selectList(getNamespace() + ".searchName", page);
			page.setResult(Commoditys);
			page.setTotalCount(getQueryCount(".searchName"));
		} else if (!Strings.isNullOrEmpty(page.getParams().get("phone").toString().replace("%", ""))) {
			List<AgencyOrg> Commoditys = getSqlSession().selectList(getNamespace() + ".searchPhone", page);
			page.setResult(Commoditys);
			page.setTotalCount(getQueryCount(".searchPhone"));
		} else if (!Strings.isNullOrEmpty(page.getParams().get("areas").toString().replace("%", ""))) {
			List<AgencyOrg> Commoditys = getSqlSession().selectList(getNamespace() + ".searchAreas", page);
			page.setResult(Commoditys);
			page.setTotalCount(getQueryCount(".searchAreas"));
		} else {
			List<AgencyOrg> Commoditys = getSqlSession().selectList(getNamespace() + ".query", page);
			page.setResult(Commoditys);
			page.setTotalCount(getQueryCount(".query"));
		}
		return page;
	}
	
	private int getQueryCount(String method) {
		return getSqlSession().selectList(getNamespace() + method).size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AgencyOrg> query() {
		return getSqlSession().selectList(getNamespace() + ".query"); 
	}
 
	@Override
	public AgencyOrg getByAccount(String username) {
		return (AgencyOrg) getSqlSession().selectOne(getNamespace() + ".getByAccount", username); 
	}

	@Override
	public void resetPassword(AgencyOrg org) {
		getSqlSession().update(getNamespace() + ".resetpassword", org);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AgencyOrg> queryHaveTagsOrgs() {
		return getSqlSession().selectList(getNamespace() + ".queryHaveTags"); 
	}

	@Override
	public AgencyOrg queryOrgByFansUserId(String weixinUserId) { 
		return (AgencyOrg) getSqlSession().selectOne(getNamespace() + ".queryOrgByFansUserId", weixinUserId);
	}

	@Override
	public AgencyOrg queryOrgByFansNick(String fansNick) {
		return (AgencyOrg) getSqlSession().selectOne(getNamespace() + ".queryOrgByFansNick", fansNick);
	}

}
