package com.linxs.topcham.infrastruture.persist.org;

import java.util.List;

import com.kissme.core.orm.Page;
import com.linxs.topcham.domain.org.AgencyOrg;

public interface AgencyOrgRepository {

	void save(AgencyOrg org);
	 
	void update(AgencyOrg org);
	
	AgencyOrg get(long id); 

	AgencyOrg findAgencyOrgByAccount(String account);
 
	Page<AgencyOrg> queryPage(Page<AgencyOrg> page);

	List<AgencyOrg> query();

	AgencyOrg getByAccount(String username);

	void resetPassword(AgencyOrg org);

	List<AgencyOrg> queryHaveTagsOrgs();

	AgencyOrg queryOrgByFansUserId(String weixinUserId);

	AgencyOrg queryOrgByFansNick(String fansNick);     
	
}
