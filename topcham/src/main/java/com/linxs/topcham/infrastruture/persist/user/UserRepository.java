package com.linxs.topcham.infrastruture.persist.user;

import com.linxs.topcham.domain.user.User;

/**
 * 
 * @author Linxs
 *
 */
public interface UserRepository {

	public void save(User user);

	public User getByUsername(String username); 

	public void update(User user);
	
}
 