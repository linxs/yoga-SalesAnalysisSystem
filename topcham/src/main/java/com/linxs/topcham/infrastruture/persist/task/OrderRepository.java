package com.linxs.topcham.infrastruture.persist.task;

import java.util.List;

import com.kissme.core.orm.Page;
import com.linxs.topcham.domain.fans.Order;

public interface OrderRepository {

	void save(Order order);

	Page<Order> queryPage(Page<Order> page);

	void saveOrderMonthRecord(Order order);

	void update(Order order);

	Order get(String id);

	List<Order> queryThisMonthOrders(String id, String recordTime);

	List<Order> query(); 

	List<Order> queryCurrentMonthAllOrder();
 
	List<String> queryCurrentMonthAllOrderIds();

	Order getOrder(String orderId);

	void updateOrderStatus(Order order);

	List<Order> queryFinishOrderOfOrg(String id);

	void updateAddress(Order tid);
 
	List<Order> queryAllOrder();

	List<Order> queryCurrentMonthPersiterData();

	List<Order> queryByMonth(String month);      

}
 