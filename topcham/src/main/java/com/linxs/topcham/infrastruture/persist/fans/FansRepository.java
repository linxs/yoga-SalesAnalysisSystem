package com.linxs.topcham.infrastruture.persist.fans;

import java.util.List;

import com.kissme.core.orm.Page;
import com.linxs.topcham.domain.fans.Fans;

public interface FansRepository {

	Fans saveFans(Fans org);
	 
	Fans get(long id);

	List<Fans> query();

	Page<Fans> queryPage(Page<Fans> page);

	int countFansWithOrg(String id);
 
	Page<Fans> queryMyFansPage(Page<Fans> page, String username);

	List<Fans> queryMyFans(String username); 

	List<Fans> queryMyFans(String username, String first, String last);

	Fans getFansByUserid(String userid); 

	void delete(String id);

	void removeAll(); 

	void activate(Fans fans); 

	boolean existActivate(String userId);

	void unactivate(Fans fans);

	Page<Fans> queryMyThisMonthFansPage(Page<Fans> page, String username);

	Fans getFansByOrgAndWeixinOpenId(String weixinOpenId, String id); 

}
