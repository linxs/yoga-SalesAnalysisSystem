package com.linxs.topcham.infrastruture.persist.kdtapi;

import java.util.List;

import com.kissme.core.orm.Page;
import com.linxs.topcham.domain.kdtapi.KdtFans;

public interface KdtApiRepository {

	void delete(); 

	void save(KdtFans fans);

	Page<KdtFans> query(Page<KdtFans> page);

	List<KdtFans> queryByTags(String tags);

	KdtFans get(String weixinOpenId); 
} 
 