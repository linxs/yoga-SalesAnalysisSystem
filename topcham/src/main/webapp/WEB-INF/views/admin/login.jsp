<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<%@include file="/commons/taglibs.jsp"%>
    <title>samyama瑜伽服销售分析系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Bootstrap -->
    <link href="${ctx }/resources/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <style>
        html {
            background: url(${ctx}/resources/images/fore/login_back.jpg) no-repeat center center fixed; 
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        body {
            padding-top: 20px;
            font-size: 16px;
            font-family: "Open Sans",serif;
            background: transparent;
        }

        h1 {
            font-family: "Abel", Arial, sans-serif;
            font-weight: 400;
            font-size: 40px;
        }

        /* Override B3 .panel adding a subtly transparent background */
        .panel {
            background-color: rgba(255, 255, 255, 0.6);
        }

    </style>
	<script src="${ctx }/resources/js/jquery.js" type="text/javascript"></script>
	<script src="${ctx }/resources/js/jquery.form.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(function() {
		
			$("#username").focus();
			
			$("#login").click(function() {
				$("form").ajaxSubmit({
					dataType : 'html',
					success : function(data) {
						if(data === "success") {
							location.href = "${ctx}/admin/index";
						} else {
							alert("帐号或密码错误");
						}
					}
				});
		
			});
			
			$("#password").bind('keydown', function(event) {
				if (event.keyCode === 13) {
					$("#login").click();
				}
			});
		});
	</script>
</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 panel" style="margin-top: 131px;">
                </br>
                <h1 style="text-align: center;">samyama瑜伽服销售分析系统</h1>
                </br>
                <form:form class="bs-docs-example form-horizontal" style="margin-left: 203px;" commandName="user">
                    <div class="control-group">
                      <label class="control-label" for="inputEmail">帐号</label>
                      <div class="controls">
                        <form:input path="username" placeholder="输入用户名..." />
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label" for="inputPassword">密码</label>
                      <div class="controls">
                         <form:password path="password" placeholder="输入登录密码..." />
                      </div>
                    </div>
                    <div class="control-group">
                      <div class="controls">
                        <a id="login" href="#none" class="btn btn-primary btn-large">登陆</a>
                      </div>
                    </div>
                  </form:form>
              </br>

            </div>
        </div>
    </div> 
    <div style="margin-top: 180px;color:white;">
	 <%@include file="/WEB-INF/common/footer.jsp"%>
    </div>
</body>
</html>
