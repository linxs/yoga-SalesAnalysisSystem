<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<%@include file="/commons/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>samyama瑜伽服销售分析系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<%@include file="/commons/no-cache.jsp"%>
	<%@include file="/commons/common-header.jsp"%>
   
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    
  </head>

  <body>

    <input type="hidden" id="currLeft" value="orgLeft"/>
    <%@include file="/WEB-INF/common/head.jsp"%>
    
    

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3" >
          <div class="well sidebar-nav" style="height:400px;">
            <%@include file="/WEB-INF/common/left.jsp"%>
          </div><!--/.well -->
        </div><!--/span-->
        <div class="span9">
          <div class="hero-unit">
            <h2>samyama瑜伽服销售分析系统</h2>
            <p></p>
            
            <c:choose>
				<c:when test="${user.role eq 'admin' }">
					<p><a href="${ctx }/admin/org" class="btn btn-primary btn-large">推广机构 »</a></p>
				</c:when>
				<c:when test="${user.role eq 'org' }">
					<p><a href="${ctx }/admin/myfans" class="btn btn-primary btn-large">瑜伽老师查询 »</a></p>
				</c:when>
			</c:choose>
            
            
            
            
          </div>
          
          
        </div>
      </div><!--/row-->

      <hr>

       <%@include file="/WEB-INF/common/footer.jsp"%>

    </div>

   

  </body>
</html>

