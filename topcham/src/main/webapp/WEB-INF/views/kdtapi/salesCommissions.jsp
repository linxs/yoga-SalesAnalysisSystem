<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<%@include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-CN" lang="zh-CN"> 
  <head>
    <meta charset="utf-8">
    <title>samyama瑜伽服销售分析系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<%@include file="/commons/no-cache.jsp"%>
	<%@include file="/commons/common-header.jsp"%>
   
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <script>
    	$(function() {
    		$.ajax({
    			url : "${ctx }/admin/salesCommissions/${user.username }",
    			dataType : "html",
    			type : "get",
    			success : function(result) {
    				$("#salesCommissions").html(result);
    			}
    		});
    		
    	});
    </script>
  </head>

  <body>
	<input type="hidden" id="currLeft" value="salesLeft"/>
	<input type="hidden" id="isGo" value="true" />
	
       <%@include file="/WEB-INF/common/head.jsp"%>


    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3" >
          <div class="well sidebar-nav" style="height:400px;">
           <%@include file="/WEB-INF/common/left.jsp"%>
          </div><!--/.well -->
        </div><!--/span-->
       <div class="span9">
          
            <fieldset>
              <legend>销售提成</legend>
              <div class="control-group">
                <label class="control-label" for="input01">本月截止到目前的销售提成（单位：元）：<b id="salesCommissions" style="color: red;font-size: 27px;">计算中，请稍后...</b></label>
              </div>

            </fieldset>
          	
          	
          	<table class="table table-bordered">
              <thead>
              	
	                <tr>
	                  <th>
                      	订单号
	                  </th>
	                  <th>粉丝昵称</th>
	                  <th>买家付款时间</th>
	                  <th>金额</th>
	                  <th>提成（单位：元）</th>
	                </tr>
              	
              </thead>
              <tbody>
                <c:forEach items="${result }" var="order">
	               <tr>
	                  <td>${order.orderId }</td>
	                  <td>${order.fansNick }</td>
	                  <td>${order.payTime }</td>
	                  <td>${order.payment }</td>
	                  <td>￥${order.commission }</td>
	                </tr>
                </c:forEach>
              </tbody>
            </table>
          	
          	
          
        </div>
      </div><!--/row-->

      <hr>

     <%@include file="/WEB-INF/common/footer.jsp"%>

    </div>

   

  </body>
</html>

