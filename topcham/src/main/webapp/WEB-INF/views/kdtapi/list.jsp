<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<%@include file="/commons/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>samyama瑜伽服销售分析系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<%@include file="/commons/no-cache.jsp"%>
	<%@include file="/commons/common-header.jsp"%>
   
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    
    <script>
    	$(function() {
    	
    		$("#search").click(function() {
    			var fansNick = $("#fansNick").val();
    			location.href = "${ctx }/admin/kdtapi?pageNo=${page.pageNo}&nick=" + encodeURI(encodeURI(fansNick));
    		});
    	
    		$("#refreshData").click(function() {
    			if(!confirm("该过程可能耗时较长，确定需要同步到最新数据吗？")) {
    				return false;
    			}
    			
    			$.ajax({
    				url : "${ctx }/admin/refreshdata",
    				type : "post",
    				dataType : "html",
    				success : function(result) {
    					if("success" == result) {
    						alert("已成功同步至最新数据！");
    						location.reload();
    					}
    				}
    			});
    			
    		});
    	
    		$("#selectAll").click(function(){
				$("input[type='checkbox']").attr("checked",this.checked);
			});
    		
    		$(".relevance").click(function() {
    			
    			var orgId = $(this).attr("lang");
    			
    			var items = [];
				$(".fans-data:checked").each(function(index, item) {
					items.push(item['value']);
				});
				if(!items.toString()) {
					alert("请先选择需要关联的粉丝");
					return false;
				}
				
				if(!confirm("确定要将所勾选的粉丝关联到该推广机构吗？这将为该推广机构添加增员金额")) {
					return false;
				}
				
				$.ajax({
					url : "${ctx}/admin/kdtapi/relevance",
					data : {
						"orgId" : orgId,
						"fansCollection" : items.join("[;]")
					},
					type : "post",
					dataType : "html",
					success : function(result) {
						if(result == "success") {
							alert("关联成功！");
							location.reload();
						} else {
							alert("关联异常，请重试！");
						}
					}
					
				});
				
    			
    		});
    	});
    </script>
  </head>

  <body>
<input type="hidden" id="currLeft" value="kdtLeft"/>
        <%@include file="/WEB-INF/common/head.jsp"%>


    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3" >
          <div class="well sidebar-nav" style="height:400px;">
          <%@include file="/WEB-INF/common/left.jsp"%>
          </div><!--/.well -->
        </div><!--/span-->
       <div class="span9 well">


          <legend>口袋通API粉丝数据</legend>

          

          <form class="form-search">
            <span >
<!--               <div class="btn-group"> -->
<!--                 <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown"> -->
<!--                   <i class="icon-magnet icon-white"></i> 关联推广机构<span class="caret"></span></button> -->
<!--                 <ul class="dropdown-menu"> -->
<%--                   <c:forEach items="${orgs }" var="org"> --%>
<%--                   <li><a href="#none" class="relevance" lang="${org.id }">${org.orgName }</a></li> --%>
<%--                   </c:forEach> --%>
<!--                 </ul> -->
<!--               </div> -->
			  <a class="btn btn-danger" href="#none" id="refreshData"><i class="icon-refresh icon-white"></i> 同步数据</a>
			  
			  
			  <span >
              <input type="text" id="fansNick" placeholder="粉丝昵称" value="${nick }">

              <div class="btn-group">
                <a class="btn btn-success" id="search"><i class="icon-search icon-white"></i> 搜索 </a>
              </div>

            </span>
			  
            </span>
            
          </form>


          <table class="table table-bordered">
              <thead>
              	
	                <tr>
	                  <th>
	                    <label class="checkbox">
	                      <input id="selectAll" type="checkbox"> 粉丝ID
	                    </label>
	                  </th>
	                  <th>头像</th>
	                  <th>昵称</th>
	                  <th>性别</th>
	                  <th>地址</th>
	                  <th>关注时间</th>
	                  <th>标识</th>
	                  <th>所属推广机构</th>
	                </tr>
              	
              </thead>
              <tbody>
                <c:forEach items="${page.result }" var="fans">
                
	                <tr>
	                  <td>
	                    <label class="checkbox">
	                    	<c:choose>
	                  			<c:when test="${not empty fans.org}">
	                  				 ${fans.userId }
	                  			</c:when>
	                  			<c:otherwise>
			                      <input class="fans-data" type="checkbox" value="${fans.weixinOpenId }[-]${fans.followTime}[-]${fans.sex}[-]${fans.nick}[-]${fans.province}[-]${fans.userId}[-]${fans.avatar eq '' ? '  ' : fans.avatar}[-]${fans.city}"> ${fans.userId }
	                  			</c:otherwise>
	                  		</c:choose>
	                  		
	                    </label>
	                  </td>
	                  <td><img src="${fans.avatar }" style="width:40px;height:40px"/></td>
	                  <td>${fans.nick }</td>
	                  <td>${fans.sex == 'm' ? '男' : '女' }</td>
	                  <td>${fans.province } ${fans.city }</td>
	                  <td>${fans.followTime }</td>
	                  <td>${fans.tags }</td>
	                  <td>
	                  		<c:choose>
	                  			<c:when test="${not empty fans.org}">
	                  				${fans.org.orgName }
	                  			</c:when>
	                  		</c:choose>
	                  </td>
	                  <input type="hidden" value="${fans.weixinOpenId }"/>
	                </tr>
                </c:forEach>
              </tbody>
            </table>
            
            	
             	<c:choose>
             		<c:when test="${not empty nick }">
			             <jsp:include page="/commons/adminPage.jsp" flush="true" >
		  			 	<jsp:param name="pageLink" value="${ctx }/admin/kdtapi?nick=${nick }" ></jsp:param>
			  		     </jsp:include>
             		</c:when>
             		<c:otherwise>
			             <jsp:include page="/commons/adminPage.jsp" flush="true" >
		  			 	<jsp:param name="pageLink" value="${ctx }/admin/kdtapi?1=1" ></jsp:param>
			  		     </jsp:include>
             		</c:otherwise>
             	</c:choose>
            
        </div>
      </div><!--/row-->

      <hr>

     <%@include file="/WEB-INF/common/footer.jsp"%>

    </div>

   

  </body>
</html>

