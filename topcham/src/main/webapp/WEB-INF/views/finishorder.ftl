<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author></Author>
  <LastAuthor></LastAuthor>
  <Created>2014-11-25T03:15:17Z</Created>
  <Company>linxs</Company>
  <Version>14.0</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>10640</WindowHeight>
  <WindowWidth>25600</WindowWidth>
  <WindowTopX>-200</WindowTopX>
  <WindowTopY>1040</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="宋体" x:CharSet="134" x:Family="Swiss" ss:Size="12"
    ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
 </Styles>
 <Worksheet ss:Name="明细">
  <Table ss:ExpandedColumnCount="21" ss:ExpandedRowCount="999999" x:FullColumns="1"
   x:FullRows="1" ss:DefaultColumnWidth="65" ss:DefaultRowHeight="15">
  <Row>
    <Cell><Data ss:Type="String">订单ID</Data></Cell>
    <Cell><Data ss:Type="String">实际支付金额</Data></Cell>
    <Cell><Data ss:Type="String">签收时间</Data></Cell>
    <Cell><Data ss:Type="String">标题</Data></Cell>
    <Cell><Data ss:Type="String">统计月份</Data></Cell>
    <Cell><Data ss:Type="String">微信ID</Data></Cell>
    <Cell><Data ss:Type="String">提成折扣</Data></Cell>
    <Cell><Data ss:Type="String">提成</Data></Cell>
    <Cell><Data ss:Type="String">所属推广机构</Data></Cell>
   </Row>

<#list orders as order>
   <Row>
    <Cell><Data ss:Type="String">${order.orderId?default(" ") } </Data></Cell>
    <Cell><Data ss:Type="String">${order.payment?default(" ") } </Data></Cell>
    <Cell><Data ss:Type="String">${order.payTime?default(" ") } </Data></Cell>
    <Cell><Data ss:Type="String">${order.title?default(" ") } </Data></Cell>
    <Cell><Data ss:Type="String">${order.recordTime?default(" ") } </Data></Cell>
    <Cell><Data ss:Type="String">${order.weixinUserId?default(" ") } </Data></Cell>
    <Cell><Data ss:Type="String">${order.base?default(" ") } </Data></Cell>
    <Cell><Data ss:Type="String">${order.commission?default(" ") } </Data></Cell>
    <Cell><Data ss:Type="String">${order.orgId?default(" ") } </Data></Cell>
   </Row>
   </#list>

  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <Print>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <HorizontalResolution>-4</HorizontalResolution>
    <VerticalResolution>-4</VerticalResolution>
   </Print>
   <PageLayoutZoom>0</PageLayoutZoom>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>10</ActiveRow>
     <ActiveCol>2</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>