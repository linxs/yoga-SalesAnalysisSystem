<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<%@include file="/commons/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>samyama瑜伽服销售分析系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<%@include file="/commons/no-cache.jsp"%>
	<%@include file="/commons/common-header.jsp"%>
   
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    
    <script type="text/javascript" src="${ctx }/resources/js/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript">
    	$(function() {
    		
    		$("#search").click(function() {
    			var orgName = $("#orgName").val();
    			var month = $("#month").val();
    			location.href = "${ctx }/admin/orgmonthrecord?pageNo=${page.pageNo}&month="+ encodeURI(encodeURI(month)) +"&orgName=" + encodeURI(encodeURI(orgName));
    		});
    	
    	});
    </script>
  </head>

  <body>
<input type="hidden" id="currLeft" value="orgmonthrecordLeft"/>
        <%@include file="/WEB-INF/common/head.jsp"%>


    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3" >
          <div class="well sidebar-nav" style="height:400px;">
            <%@include file="/WEB-INF/common/left.jsp"%>
          </div><!--/.well -->
        </div><!--/span-->
       <div class="span9 well">


          <legend>推广机构月度费用统计</legend>

          

          <form class="form-search">

            <span >
              <input type="text" id="orgName" value="${orgName }" placeholder="推广机构名称">
              <input type="text" id="month" value="${month }" placeholder="月份" onfocus="WdatePicker({skin:'whyGreen',dateFmt:'yyyy-MM'})" class="Wdate">
                <a class="btn btn-success" id="search" href="#none" >
                  <i class="icon-search icon-white"></i> 搜索 </a>
            </span>
            
          </form>


          <table class="table table-bordered">
              <thead>
              	
	                <tr>
	                  <th>推广机构名称</th>
	                  <th>统计年月</th>
	                  <th>增员金额（单位：元）</th>
	                  <th>销售提成（单位：元）</th>
	                </tr>
              	
              </thead>
              <tbody>
                <c:forEach items="${page.result }" var="month">
	                <tr>
	                  <td>${month.org.orgName }</td>
	                  <td>${month.month }</td>
	                  <td style="color:red">${month.addpersonMoney }</td>
	                  <td style="color:red">${month.saleCommission }</td>
	                </tr>
                </c:forEach>
              </tbody>
            </table>
            
             
  		     
  		     <c:choose>
             		<c:when test="${not empty nick }">
			             <jsp:include page="/commons/adminPage.jsp" flush="true" >
		  			 	<jsp:param name="pageLink" value="${ctx }/admin/orgmonthrecord?orgName=${orgName }" ></jsp:param>
			  		     </jsp:include>
             		</c:when>
             		<c:otherwise>
			             <jsp:include page="/commons/adminPage.jsp" flush="true" >
			  			 	<jsp:param name="pageLink" value="${ctx }/admin/orgmonthrecord?1=1&month=${month }" ></jsp:param>
			  		     </jsp:include>
             		</c:otherwise>
             	</c:choose>
          
        </div>
      </div><!--/row-->

      <hr>

     <%@include file="/WEB-INF/common/footer.jsp"%>

    </div>

   

  </body>
</html>

