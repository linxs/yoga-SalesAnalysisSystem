<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<%@include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang='zh-CN' xml:lang='zh-CN' xmlns='http://www.w3.org/1999/xhtml'>

  <head>
    <meta charset="utf-8">
    <title>samyama瑜伽服销售分析系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<%@include file="/commons/no-cache.jsp"%>
	 <link href="${ctx }/resources/css/bootstrap/bootstrap.css" rel="stylesheet">
<script src="${ctx }/resources/js/plugins/easy-upload/jquery.js" type="text/javascript"></script>
<script src="${ctx}/resources/js/bootstrap/bootstrap.min.js"></script>

   
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <script type="text/javascript" src="${ctx }/resources/js/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript">
    	$(function() {
    	
    		$("#export").click(function() {
    			window.open("${ctx }/admin/ordermonthexport?month=" + $("#monthHidden").val());
    		});
    		
    		$("#search").click(function() {
    			var orderId = $("#orderId").val();
    			var month = $("#month").val();
    			
    			location.href = "${ctx }/admin/ordermonthrecord?orderid=" + orderId + "&month=" + month;
    		});
    		
    		$(".cancelPrice").live("click", function() {
    			var id = $(this).attr("id");
    			var price = $("#input" + id).val();
    			$("#input" + id).after(price);
    			$("#input" + id).remove();
    			$(".savePrice").remove();
    			$(".cancelPrice").remove();
    			
    			$(".editPrice[id='"+ id +"']").attr("style", "");
    			
    		});
    		
    		$(".savePrice").live("click", function() {
    			var id = $(this).attr("id");
    			var price = $("#input" + id).val();
    			$.ajax({
    				url : "${ctx }/admin/recountcommission",
    				data : {
    					id : id,
    					price : price
    				},
    				dataType : "html",
    				type : "post",
    				success : function(result) {
    					if("success" == result) {
    						alert("保存成功，销售提成已重新计算完毕!");
    						location.reload();
    					} else {
    						alert("保存失败，请刷新重试！");
    					}
    				}
    			});
    		});
    		
    		$(".editPrice").click(function() {
    			var id = $(this).attr("id");
    			var price = $("#paymentCommission" + id).html();
    			$("#paymentCommission" + id).html("<input id='input"+ id +"' style='width:50px;' type='text' value='"+ price +"' />");
    			$(this).after("<a id='"+ id +"' class='btn btn-success cancelPrice' href='#none' >取消</a>");
    			$(this).after("<a id='"+ id +"' class='btn btn-primary savePrice' href='#none' >保存</a>");
    			$(this).attr("style", "display:none;");
    		});
    	
    	});
    </script>
  </head>

  <body>
<input type="hidden" id="currLeft" value="ordermonthrecordLeft"/>
        <%@include file="/WEB-INF/common/head.jsp"%>


    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3" >
          <div class="well sidebar-nav" style="height:400px;">
            <%@include file="/WEB-INF/common/left.jsp"%>
          </div><!--/.well -->
        </div><!--/span-->
       <div class="span9 well">


          <legend>月份订单交易记录</legend>

          <input type="hidden" value="${month }" id="monthHidden" />

          <form class="form-search">
			
<span >
              订单编号：<input type="text" id="orderId" placeholder="订单编号" value="${orderid }">
              统计月份:<input type="text" id="month" value="${month }" placeholder="月份" onfocus="WdatePicker({skin:'whyGreen',dateFmt:'yyyy-MM'})" class="Wdate">
                <a class="btn btn-success " href="#none;" id="search">
                  <i class="icon-search icon-white"></i>  搜索 </a>
                <a class="btn btn-success " href="#none;" id="export">
                  <i class="icon-arrow-down icon-white"></i>  导出EXL </a>

            </span>
            
          </form>


          <table class="table table-bordered">
              <thead>
              	
	                <tr>
	                  <th style="white-space:nowrap">订单编号</th>
	                  <th style="white-space:nowrap">标题</th>
	                  <th style="white-space:nowrap">昵称</th>
	                  <th style="white-space:nowrap">实际价格</th>
	                  <th style="white-space:nowrap">提成金额</th>
	                  <th style="white-space:nowrap">签收时间</th>
	                  <th style="white-space:nowrap">状态</th>
	                  <th style="white-space:nowrap">记录月份</th>
<!-- 	                  <th style="white-space:nowrap">操作</th> -->
	                </tr>
              	
              </thead>
              <tbody>
                <c:forEach items="${page.result }" var="order">
	                <tr>
	                  <td style="white-space:nowrap">${order.orderId }</td>
	                  <td>${order.title }</td>
	                  <td style="white-space:nowrap">${order.fansNick }</td>
	                  <td style="white-space:nowrap">${order.payment }</td>
	                  <td style="white-space:nowrap" id="paymentCommission${order.id }">${order.commission }</td>
	                  <td style="white-space:nowrap">${order.payTime }</td>
	                  <td style="white-space:nowrap">
	                  	${order.status}
	                  </td>
	                  <td style="white-space:nowrap">
	                  	<fmt:parseDate value="${order.recordTime}" var="date" pattern="yyyy-MM-dd HH:mm:ss" />
	                  	<fmt:formatDate value="${date}" pattern="yyyy-MM"/> 
	                  	</td>
<%-- 	                  <td style="white-space:nowrap"><a href="#none" id="${order.id }" class="btn btn-info editPrice"><i class="icon-pencil icon-white"></i> 修改提成金额</a></td> --%>
	                </tr>
                </c:forEach>
              </tbody>
            </table>
            
             <jsp:include page="/commons/adminPage.jsp" flush="true" >
  			 	<jsp:param name="pageLink" value="${ctx }/admin/ordermonthrecord?1=1&month=${month }" ></jsp:param>
  		     </jsp:include>
          
        </div>
      </div><!--/row-->

      <hr>

    <%@include file="/WEB-INF/common/footer.jsp"%>

    </div>

   

  </body>
</html>

