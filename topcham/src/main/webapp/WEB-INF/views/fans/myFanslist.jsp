<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<%@include file="/commons/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>samyama瑜伽服销售分析系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<%@include file="/commons/no-cache.jsp"%>
	<%@include file="/commons/common-header.jsp"%>
   
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    
  </head>

  <body>
<input type="hidden" id="currLeft" value="myfansLeft"/>
        <%@include file="/WEB-INF/common/head.jsp"%>


    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3" >
          <div class="well sidebar-nav" style="height:400px;">
           <%@include file="/WEB-INF/common/left.jsp"%>
          </div><!--/.well -->
        </div><!--/span-->
       <div class="span9 well">


          <legend>瑜伽老师查询</legend>

          <form class="form-search">

            <span >
              <input type="text" placeholder="搜索">

              <div class="btn-group">
                <button class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                  <i class="icon-search icon-white"></i>
                  搜索 <span class="caret"></span></button>
              </div>

            </span>
            
          </form>


          <table class="table table-bordered">
              <thead>
              	
	                <tr>
	                  <th>
	                    <label class="checkbox">
	                      <input type="checkbox"> 粉丝ID
	                    </label>
	                  </th>
	                  <th>粉丝头像</th>
	                  <th>粉丝昵称</th>
	                  <th>粉丝性别</th>
	                  <th>粉丝地址</th>
	                  <th>粉丝关注时间</th>
	                  <th>推广时间</th>
	                </tr>
              	
              </thead>
              <tbody>
                <c:forEach items="${page.result }" var="fans">
	               <tr>
	                  <td>
	                    <label class="checkbox">
	                      <input class="fans-data" type="checkbox"> ${fans.userId }
	                    </label>
	                  </td>
	                  <td><img src="${fans.avatar }" style="width:40px;height:40px"/></td>
	                  <td>${fans.nick }</td>
	                  <td>${fans.sex == 'm' ? '男' : '女' }</td>
	                  <td>${fans.province } ${fans.city }</td>
	                  <td>${fans.followTime }</td>
	                  <td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${fans.createTime}" type="both"/></td>
	                </tr>
                </c:forEach>
              </tbody>
            </table>
            
             <jsp:include page="/commons/adminPage.jsp" flush="true" >
  			 	<jsp:param name="pageLink" value="${ctx }/admin/myfans?1=1" ></jsp:param>
  		     </jsp:include>
          
        </div>
      </div><!--/row-->

      <hr>

      <%@include file="/WEB-INF/common/footer.jsp"%>

    </div>

   

  </body>
</html>

