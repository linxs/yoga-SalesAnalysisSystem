<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<%@include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-CN" lang="zh-CN"> 
  <head>
    <meta charset="utf-8">
    <title>samyama瑜伽服销售分析系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<%@include file="/commons/no-cache.jsp"%>
	<%@include file="/commons/common-header.jsp"%>
   
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <script>
    	$(function() {
    		$("#refreshData").click(function() {
    			if(!confirm("确定要同步至最新数据吗？该操作耗时较长，请耐心等待。")) {
    				return false;
    			}
    			
    			$.ajax({
    				url : "${ctx }/admin/fans/refreshmydata",
    				type : "post",
    				dataType : "html",
    				success : function(result) {
    					if("success" == result) {
    						alert("同步数据成功！");
    						location.reload();
    					}
    				}
    			});
    		});
    		
    	});
    </script>
  </head>

  <body>
	<input type="hidden" id="currLeft" value="addpersonLeft"/>
	<input type="hidden" id="isGo" value="true" />
	
        <%@include file="/WEB-INF/common/head.jsp"%>


    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3" >
          <div class="well sidebar-nav" style="height:400px;">
           <%@include file="/WEB-INF/common/left.jsp"%>
          </div><!--/.well -->
        </div><!--/span-->
       <div class="span9">
          
            <fieldset>
              <legend>当月增员推广费用</legend>
<!--               <div class="control-group"> -->
<!-- 	              <a class="btn btn-danger" href="#none" id="refreshData"><i class="icon-refresh icon-white"></i> 同步数据</a> -->
<!--               </div> -->
              <div class="control-group">
                <label class="control-label" for="input01">本月截止到目前的增员人数：<b id="salesCommissions" style="font-size: 27px;">${fn:length(result) }</b></label>
              </div>

              <div class="control-group">
                <label class="control-label" for="input01">本月截止到目前的增员费用（单位：元）：<b id="salesCommissions" style="color: red;font-size: 27px;">${fn:length(result) * 0 }</b></label>
              </div>
              
              
            </fieldset>
          
          	 <table class="table table-bordered">
              <thead>
              	
	                <tr>
	                  <th>
	                    <label class="checkbox">
	                      <input type="checkbox"> 粉丝ID
	                    </label>
	                  </th>
	                  <th>粉丝头像</th>
	                  <th>粉丝昵称</th>
	                  <th>粉丝性别</th>
	                  <th>粉丝地址</th>
	                  <th>粉丝关注时间</th>
	                  <th>推广时间</th>
	                  <th>推广费用（单位：元）</th>
	                </tr>
              	
              </thead>
              <tbody>
                <c:forEach items="${page.result }" var="fans">
	               <tr>
	                  <td>
	                    <label class="checkbox">
	                      <input class="fans-data" type="checkbox"> ${fans.userId }
	                    </label>
	                  </td>
	                  <td><img src="${fans.avatar }" style="width:40px;height:40px"/></td>
	                  <td>${fans.nick }</td>
	                  <td>${fans.sex == 'm' ? '男' : '女' }</td>
	                  <td>${fans.province } ${fans.city }</td>
	                  <td>${fans.followTime }</td>
	                  <td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${fans.createTime}" type="both"/></td>
	                  <td>￥0.00</td>
	                </tr>
                </c:forEach>
              </tbody>
            </table>
            
             <jsp:include page="/commons/adminPage.jsp" flush="true" >
  			 	<jsp:param name="pageLink" value="${ctx }/admin/addpersonmoney?1=1" ></jsp:param>
  		     </jsp:include>
          
        </div>
      </div><!--/row-->

      <hr>

      <%@include file="/WEB-INF/common/footer.jsp"%>

    </div>

   

  </body>
</html>

