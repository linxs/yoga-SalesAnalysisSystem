<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<%@include file="/commons/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>samyama瑜伽服销售分析系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<%@include file="/commons/no-cache.jsp"%>
	<%@include file="/commons/common-header.jsp"%>
   
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <script>
    	$(function() {
    		
    		$("#refreshData").click(function() {
    			if(!confirm("确定要同步至最新数据吗？该操作耗时较长，请耐心等待。")) {
    				return false;
    			}
    			
    			$.ajax({
    				url : "${ctx }/admin/fans/refreshdata",
    				type : "post",
    				dataType : "html",
    				success : function(result) {
    					if("success" == result) {
    						alert("同步数据成功！");
    						location.reload();
    					}
    				}
    			});
    		});
    		
    		$("#search").click(function() {
    			var nick = $("#nick").val();
    			
    			location.href = "${ctx }/admin/fans?nick=" + encodeURI(encodeURI(nick));
    		});
    		
    		$(".activate").click(function() {
    			var id = $(this).attr("lang");
    			
    			$.ajax({
    				url : "${ctx }/admin/fans/activate?id=" + id,
    				type : "post",
    				dataType : "html",
    				success : function(result) {
    					if("success" == result) {
    						alert("激活成功！");
    						location.reload();
    					} else {
    						alert("操作异常，请刷新页面重试");
    					}
    				}
    			});
    		});

    		$(".unactivate").click(function() {
    			if(!confirm("确定要解除该激活状态吗？")) {
    				return false;
    			}
    		
    			var id = $(this).attr("lang");
    			
    			$.ajax({
    				url : "${ctx }/admin/fans/unactivate?id=" + id,
    				type : "post",
    				dataType : "html",
    				success : function(result) {
    					if("success" == result) {
    						alert("已成功解除！");
    						location.reload();
    					} else {
    						alert("操作异常，请刷新页面重试");
    					}
    				}
    			});
    		});
    	});
    </script>
  </head>

  <body>
		<input type="hidden" id="currLeft" value="fansLeft"/>
        <%@include file="/WEB-INF/common/head.jsp"%>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3" >
          <div class="well sidebar-nav" style="height:400px;">
           <%@include file="/WEB-INF/common/left.jsp"%>
          </div><!--/.well -->
        </div><!--/span-->
       <div class="span9 well">


          <legend>已关联的粉丝</legend>

          <form class="form-search">
			<span >
			  
              粉丝昵称：<input type="text" id="nick" placeholder="粉丝昵称" value="${nick }">
                <a class="btn btn-success " href="#none;" id="search">
                  <i class="icon-search icon-white"></i>  搜索 </a>

			<a class="btn btn-danger" href="#none" id="refreshData"><i class="icon-refresh icon-white"></i> 同步数据</a>
            </span>
            
          </form>


          <table class="table table-bordered">
              <thead>
              	
	                <tr>
	                  <th>
	                    <label class="checkbox">
	                      <input type="checkbox"> 粉丝ID
	                    </label>
	                  </th>
	                  <th>粉丝头像</th>
	                  <th>粉丝昵称</th>
	                  <th>性别</th>
	                  <th>粉丝地址</th>
	                  <th>粉丝关注时间</th>
	                  <th>所属推广机构</th>
	                  <th>状态</th>
	                </tr>
              	
              </thead>
              <tbody>
                <c:forEach items="${page.result }" var="fans">
	               <tr>
	                  <td>
	                    <label class="checkbox">
	                      <input class="fans-data" type="checkbox"> ${fans.userId }
	                    </label>
	                  </td>
	                  <td><img src="${fans.avatar }" style="width:40px;height:40px"/></td>
	                  <td>${fans.nick }</td>
	                  <td>${fans.sex == 'm' ? '男' : '女' }</td>
	                  <td>${fans.province } ${fans.city }</td>
	                  <td>${fans.followTime }</td>
	                  <td>${fans.org.orgName }</td>
	                  <td>
	                  	<c:choose>
	                  		<c:when test="${fans.status eq '0' }">
			                  	<a href="#none" lang="${fans.id }" class="btn btn-primary activate" ><i class="icon-ok-sign icon-white"></i> 激活</a>
	                  		</c:when>
	                  		<c:when test="${fans.status eq '1' }">
			                  	<a href="#none" lang="${fans.id }" class="btn btn-danger unactivate" ><i class="icon-ban-circle icon-white"></i> 解除</a>
	                  		</c:when>
	                  	</c:choose>
	                  </td>
	                  <input type="hidden" value="${fans.weixinOpenId }"/>
	                </tr>
                </c:forEach>
              </tbody>
            </table>
            
             <jsp:include page="/commons/adminPage.jsp" flush="true" >
  			 	<jsp:param name="pageLink" value="${ctx }/admin/fans?1=1" ></jsp:param>
  		     </jsp:include>
          
        </div>
      </div><!--/row-->

      <hr>

       <%@include file="/WEB-INF/common/footer.jsp"%>
      
    </div>

   

  </body>
</html>

