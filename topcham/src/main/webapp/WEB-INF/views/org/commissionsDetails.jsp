<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<%@include file="/commons/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>samyama瑜伽服销售分析系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<%@include file="/commons/no-cache.jsp"%>
	<%@include file="/commons/common-header.jsp"%>
   
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <script type="text/javascript">
    	$(function() {
			    	
    	});
    </script>
  </head>

  <body>
<input type="hidden" id="currLeft" value="orgLeft"/>
        <%@include file="/WEB-INF/common/head.jsp"%>


    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3" >
          <div class="well sidebar-nav" style="height:400px;">
            <%@include file="/WEB-INF/common/left.jsp"%>
          </div><!--/.well -->
        </div><!--/span-->
       <div class="span9 well">


          <legend>${org.orgName }当月销售签收订单</legend>

          

          <form class="form-search">

            <span >
              <%-- 名称：<input type="text" id="orgName" style="width:180px" placeholder="推广机构名称" value="${name }">
              电话：<input type="text" id="phone" style="width:180px" placeholder="联系电话" value="${phone }">
              地区：<input type="text" id="areas" style="width:180px" placeholder="代理区域" value="${areas }">
                <a class="btn btn-success " href="#none;" id="search">
                  <i class="icon-search icon-white"></i>  搜索 </a> --%>


			<a class="btn btn-primary" href="${ctx }/admin/org">
              <i class="icon-arrow-left icon-white"></i> 返回推广机构
            </a>

            <a class="btn btn-success" href="${ctx }/admin/commissions-export/${org.id }">
              <i class="icon-arrow-down icon-white"></i> 导出Excel
            </a>
            </span>
            
          </form>


          <table class="table table-bordered">
              <thead>
              	
	                <tr>
	                  <th>订单号</th>
	                  <th>粉丝昵称</th>
	                  <th>收货人</th>
	                  <th>收货人电话</th>
	                  <th>买家付款时间</th>
	                  <th>金额</th>
	                  <th>提成</th>
	                </tr>
              	
              </thead>
              <tbody>
                <c:forEach items="${orders }" var="order">
	                <tr>
	                
	                  <td style="white-space:nowrap">${order.orderId }</td>
	                  <td style="white-space:nowrap">${order.fansNick }</td>
	                  <td style="white-space:nowrap">${order.receiverName }</td>
	                  <td style="white-space:nowrap">${order.receiverMobile }</td>
	                  <td style="white-space:nowrap">${order.payTime }</td>
	                  <td style="white-space:nowrap">${order.payment }</td>
	                  <td style="white-space:nowrap">${order.commission }</td>
	                
	                </tr>
                </c:forEach>
              </tbody>
            </table>
            
             <jsp:include page="/commons/adminPage.jsp" flush="true" >
  			 	<jsp:param name="pageLink" value="${ctx }/admin/org?1=1" ></jsp:param>
  		     </jsp:include>
          
        </div>
      </div><!--/row-->

      <hr>

     <%@include file="/WEB-INF/common/footer.jsp"%>

    </div>

   

  </body>
</html>

