<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<%@include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-CN" lang="zh-CN"> 
  <head>
    <meta charset="utf-8">
    <title>samyama瑜伽服销售分析系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<%@include file="/commons/no-cache.jsp"%>
	<%@include file="/commons/common-header.jsp"%>
   
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <script>
    	$(function() {
    		$("#submit").click(function() {
    			var password = $("#password").val();
    			if("" == password) {
    				alert("密码不允许为空！");
    				return false;
    			}
    			$.ajax({
    				url : "${ctx }/admin/resetpassword",
    				data : {
    					"password" : password 
    				},
    				type : "post",
    				dataType : "html",
    				success : function(result) {
    					if("success" == result) {
    						alert("密码修改成功！请重新登录");
    						location.href = "${ctx }/admin/sign-out";
    					}
    				}
    			});
    		});
    		
    	});
    </script>
  </head>

  <body>
	<input type="hidden" id="currLeft" value="resetPasswordLeft"/>
	<input type="hidden" id="isGo" value="true" />
	
       <%@include file="/WEB-INF/common/head.jsp"%>


    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3" >
          <div class="well sidebar-nav" style="height:400px;">
           <%@include file="/WEB-INF/common/left.jsp"%>
          </div><!--/.well -->
        </div><!--/span-->
       <div class="span9">
          
            <fieldset>
              <legend>密码修改</legend>
              <div class="control-group">
                <label class="control-label" for="input01">新密码</label>
                <div class="controls">
                  <input type="password" class="input-xlarge" id="password" name="password" placeholder="输入新密码"/>
                </div>
              </div>
              <div class="form-actions">
                <button type="button" id="submit" class="btn btn-large btn-primary">确   定</button>
                <div style="display:none">
	                <button type="submit" id="hiddenSubmit">确   定</button>
                </div>
              </div>
            </fieldset>
          
          
        </div>
      </div><!--/row-->

      <hr>

      <%@include file="/WEB-INF/common/footer.jsp"%>

    </div>

   

  </body>
</html>

