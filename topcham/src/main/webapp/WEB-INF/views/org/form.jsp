<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<%@include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-CN" lang="zh-CN"> 
  <head>
    <meta charset="utf-8">
    <title>samyama瑜伽服销售分析系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<%@include file="/commons/no-cache.jsp"%>
	<%@include file="/commons/common-header.jsp"%>
   
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <script>
    	$(function() {
    	
    		$("#submit").click(function() {
    			var isGo = $("#isGo").val();
    			if("true" == isGo) {
    				$("#hiddenSubmit").click();
    			} else {
    				return false;
    			}
    		});
    		
    		$("#account").change(function() {
    			$("#isGo").attr("value", "true");
    		});
    		
    		$("#account").blur(function() {
    			$.ajax({
    				url : "${ctx}/admin/org/exists/" + $(this).val(), 
    				type : "get", 
    				dataType : "html",
    				success : function(result) {
    					if("exists" === result) {
    						alert("该用户名已经存在，请更换！");
    						$("#isGo").attr("value", "false");
    						return false;
    					}
    				}
    			});
    		});
    	});
    </script>
  </head>

  <body>
		<input type="hidden" id="currLeft" value="orgLeft"/>
	<input type="hidden" id="isGo" value="true" />
	
        <%@include file="/WEB-INF/common/head.jsp"%>


    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3" >
          <div class="well sidebar-nav" style="height:400px;">
           <%@include file="/WEB-INF/common/left.jsp"%>
          </div><!--/.well -->
        </div><!--/span-->
       <div class="span9">
          <form:form id="cms-company-contact-form" class="form-horizontal" commandName="org"> 
          
            <fieldset>
              <legend>推广机构</legend>
              <div class="control-group">
                <label class="control-label" for="input01">推广机构名称</label>
                <div class="controls">
                  <form:input path="orgName" class="input-xlarge" placeholder="推广机构名称" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="input01">帐号</label>
                <div class="controls">
                  <form:input path="account" id="account" class="input-xlarge" placeholder="推广机构登录帐号" />（备注：只能使用英文作为帐号）
                </div>
              </div>
              <c:if test="${empty org.id }">
              <div class="control-group">
                <label class="control-label" for="input01">密码</label>
                <div class="controls">
                  <form:password path="password" class="input-xlarge" placeholder="推广机构登录密码" />
                </div>
              </div>
              </c:if>
              <div class="control-group">
                <label class="control-label" for="input01">联系人</label>
                <div class="controls">
                  <form:input path="contactMan" class="input-xlarge" placeholder="推广机构联系人" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="input01">邮箱</label>
                <div class="controls">
                  <form:input path="email" class="input-xlarge" placeholder="推广机构邮箱" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="input01">手机</label>
                <div class="controls">
                  <form:input path="phone" class="input-xlarge" placeholder="推广机构联系电话" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="input01">地址</label>
                <div class="controls">
                  <form:input path="address" class="input-xlarge" placeholder="推广机构联系地址" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="input01">代理区域</label>
                <div class="controls">
                  <form:input path="areas" class="input-xlarge" placeholder="推广机构代理区域" /> （备注：“全国地区” 5%、“城市名” 5%， 默认为3%）
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="input01">粉丝标识</label>
                <div class="controls">
                  <form:input path="tags" class="input-xlarge" placeholder="请与口袋通粉丝tags保持一致" /> 
                </div>
              </div>
              <div class="form-actions">
              	<form:hidden path="status" value="1"/> 
                <button type="button" id="submit" class="btn btn-large btn-primary">保  存</button>
                <button type="button" class="btn btn-large btn-success">取  消</button>
                <div style="display:none">
	                <button type="submit" id="hiddenSubmit">保  存</button>
                </div>
              </div>
            </fieldset>
          </form:form>
          
          
        </div>
      </div><!--/row-->

      <hr>

      <%@include file="/WEB-INF/common/footer.jsp"%>

    </div>

   

  </body>
</html>

