<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<%@include file="/commons/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>samyama瑜伽服销售分析系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<%@include file="/commons/no-cache.jsp"%>
	<%@include file="/commons/common-header.jsp"%>
   
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <script type="text/javascript">
    	$(function() {
    	
    		$("#search").click(function() {
    			var name = $("#orgName").val();
    			var phone = $("#phone").val();
    			var areas = $("#areas").val();
    			
    			location.href = "${ctx }/admin/org?name=" + encodeURI(encodeURI(name)) + "&phone=" + encodeURI(encodeURI(phone)) + "&areas=" + encodeURI(encodeURI(areas));
    		});
    	
    		$(".cms-delete").click(function() {
    			var id = $(this).attr("lang");
    			if(!confirm("确定要删除该推广机构吗？")) {
    				return false;
    			}
    			
    			$.ajax({
    				url : "${ctx}/admin/org/delete/" + id,
    				type : "post",
    				dataType : "html",
    				success : function(result) {
    					if("success" == result) {
    						alert("删除成功！");
    						location.reload();
    					}
    				}
    			});
    		});
    		
    		$(".cms-defaultPassword").click(function() {
    			var id = $(this).attr("lang");
    			if(!confirm("确定要重置该推广机构的密码吗？")) {
    				return false;
    			}
    			
    			$.ajax({
    				url : "${ctx }/admin/org/defaultpass",
    				data : {
    					id : id
    				},
    				type : "post",
    				dataType : "html",
    				success : function(result) {
    					alert("密码成功重置为123456");
    				}
    			});
    		});
    	
    		$(".salesCommissions").each(function() {
    			var account = $(this).attr("lang");
    			$.ajax({
    				url : "${ctx }/admin/salesCommissions/" + account,
    				type : "get",
    				dataType : "html",
    				success : function(result) {
    					$("label[lang='"+ account +"']").html(result);
    				}
    			});
    		});
    	});
    </script>
  </head>

  <body>
<input type="hidden" id="currLeft" value="orgLeft"/>
        <%@include file="/WEB-INF/common/head.jsp"%>


    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3" >
          <div class="well sidebar-nav" style="height:400px;">
            <%@include file="/WEB-INF/common/left.jsp"%>
          </div><!--/.well -->
        </div><!--/span-->
       <div class="span9 well">


          <legend>推广机构管理</legend>

          

          <form class="form-search">

            <span >
              名称：<input type="text" id="orgName" style="width:140px" placeholder="推广机构名称" value="${name }">
              电话：<input type="text" id="phone" style="width:140px" placeholder="联系电话" value="${phone }">
              地区：<input type="text" id="areas" style="width:140px" placeholder="代理区域" value="${areas }">
                <a class="btn btn-success " href="#none;" id="search">
                  <i class="icon-search icon-white"></i>  搜索 </a>

            
            <a class="btn btn-primary" href="${ctx }/admin/org/create">
              <i class="icon-plus icon-white"></i> 添加推广机构
            </a>
            </span>
            
          </form>


          <table class="table table-bordered">
              <thead>
              	
	                <tr>
	                  <th>推广机构名称</th>
	                  <th>推广机构帐号</th>
	                  <th>地区</th>
	                  <th>联系电话</th>
<!-- 	                  <th>邮箱</th> -->
	                  <th>增员金额(当月)</th>
	                  <th>销售提成（当月）</th>
	                  <th width="20%">操作</th>
	                </tr>
              	
              </thead>
              <tbody>
                <c:forEach items="${page.result }" var="org">
	                <tr>
	                  <td style="white-space:nowrap">${org.orgName }</td>
	                  <td style="white-space:nowrap">${org.account }</td>
	                  <td style="white-space:nowrap">${org.areas }</td>
	                  <td style="white-space:nowrap">${org.phone }</td>
<%-- 	                  <td>${org.email }</td> --%>
	                  <td style="white-space:nowrap">${org.addPersonMoney }</td>
	                  <td style="white-space:nowrap"><a href="${ctx }/admin/commissions-details/${org.id}"><label class="salesCommissions" lang="${org.account }">计算中，请稍后...</label></a></td>
	                  <td style="white-space:nowrap">
	                    <a href="${ctx }/admin/org/update/${org.id}" class="btn btn-info"><i class="icon-pencil icon-white"></i> 编辑</a>
	                    <a href="#none" class="btn btn-danger cms-delete" lang="${org.id }"><i class="icon-trash icon-white"></i> 删除</a>
	                    <a href="#none" class="btn btn-danger cms-defaultPassword" lang="${org.id }"><i class="icon-repeat icon-white"></i> 密码重置</a>
	                  </td>
	                </tr>
                </c:forEach>
              </tbody>
            </table>
            
             <jsp:include page="/commons/adminPage.jsp" flush="true" >
  			 	<jsp:param name="pageLink" value="${ctx }/admin/org?1=1" ></jsp:param>
  		     </jsp:include>
          
        </div>
      </div><!--/row-->

      <hr>

     <%@include file="/WEB-INF/common/footer.jsp"%>

    </div>

   

  </body>
</html>

