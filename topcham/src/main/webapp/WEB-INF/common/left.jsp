<%@include file="/commons/taglibs.jsp" %>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>

<script>
	$(function() {
		$("li[class='active']").removeClass("active");
		var currLeft = $("#currLeft").val();
		$("#" + currLeft).addClass("active"); 
	});
</script>

<c:choose>
	<c:when test="${user.role eq 'admin' }">
		<ul class="nav nav-list">
			<li class="nav-header">推广机构管理</li>
			<li id="orgLeft" ><a href="${ctx }/admin/org">推广机构</a></li>
			<li class="nav-header">粉丝管理</li>
			<li id="kdtLeft"><a href="${ctx }/admin/kdtapi">口袋通API粉丝数据</a></li>
			<li id="fansLeft"><a href="${ctx }/admin/fans">已关联的粉丝</a></li>
			<li class="nav-header">业务查询</li>
			<li id="orderLeft"><a href="${ctx }/admin/order">当前已签收订单查询</a></li>
			<li id="orgmonthrecordLeft"><a href="${ctx }/admin/orgmonthrecord">月份推广费用统计</a></li>
			<li id="ordermonthrecordLeft"><a href="${ctx }/admin/ordermonthrecord">月份订单交易记录</a></li>
			<li class="nav-header">信息安全</li>
			<li id="resetPasswordLeft"><a href="${ctx }/admin/resetpassword">密码修改</a></li>
		</ul>

	</c:when>
	<c:when test="${user.role eq 'org' }">
		
		<ul class="nav nav-list">
			<li class="nav-header">业务查询</li>
			<li id="myfansLeft"><a href="${ctx }/admin/myfans">瑜伽老师查询</a></li>
			<li class="nav-header">提成查询</li>
			<li id="addpersonLeft"><a href="${ctx }/admin/addpersonmoney">当月增员推广费用查询</a></li>
			<li id="salesLeft"><a href="${ctx }/admin/salesCommissions">当月销售提成查询</a></li>
			<li id="orgmonthrecordLeft"><a href="${ctx }/admin/mymonthrecord">月份总金额查询</a></li>
			<li class="nav-header">信息安全</li>
			<li id="resetPasswordLeft"><a href="${ctx }/admin/resetpassword">密码修改</a></li>
		</ul>
	</c:when>
</c:choose>

