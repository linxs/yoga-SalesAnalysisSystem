<%@include file="/commons/taglibs.jsp" %>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="${ctx }/admin/index">samyama瑜伽服销售分析系统</a>
          <div class="nav-collapse collapse">
            <p class="navbar-text pull-right">
          你好，
          <a href="#" class="navbar-link">${user.username }</a>
          <a href="${ctx }/admin/sign-out" class="navbar-link">注销</a>
            </p>
<!--             <ul class="nav"> -->
<!--               <li class="active"><a href="#">Home</a></li> -->
<!--               <li><a href="#about">About</a></li> -->
<!--               <li><a href="#contact">Contact</a></li> -->
<!--             </ul> -->
          </div>
        </div>
      </div>
    </div>